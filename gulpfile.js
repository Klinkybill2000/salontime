const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

// Disable sourcemaps
elixir.config.sourcemaps = false;

// Node package manager root path 
function npm(vendor) {
    return './node_modules/' + vendor;
}

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss')
       .webpack('app.js');

    mix.scripts([
        npm('jquery/dist/jquery.js'),
        npm('bootstrap-sass/assets/javascripts/bootstrap.js'),
        'vendor/modernizr.js',
        'vendor/jquery.validate.js',
        'vendor/moment.js',
        'vendor/pace.js',
        'vendor/jquery.scrollbar.js',
        'vendor/bootstrap-datepicker.js',
        'vendor/fullcalendar.js',
        'vendor/dropzone.js',
        'vendor/jquery.bootstrap.wizard.js',
    ], 'public/js/vendor.js');

    mix.scripts('vendor/pages.js', 'public/js/pages.js');
});
