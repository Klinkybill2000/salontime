<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['salon_id', 'name', 'email', 'phone', 'note'];

    /**
     * Get the Salongs that Employee belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salon()
    {
        return $this->belongsTo(salon::class);
    }

    /**
     * Get appoinments that are booked at the specefied salon.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    /**
     * Book an appointment.
     *
     * @param array $data
     * @return $this
     */
    public function book(array $data)
    {
        $appointment = new Appointment($data);

        $this->appointments()->save($appointment);

        return $this;
    }
}
