<?php

namespace App\Http\Requests;

use App\Service;
use Illuminate\Foundation\Http\FormRequest;

class UpdateService extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
//        $service = Service::findOrFail($this->route('service'));
//
//        return $service && $this->user()->can('service-update', $service);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'price' => 'required|integer|min:0',
            'duration' => 'required|integer|min:0',
            'target' => 'integer|min:0',
            'buffer_before' => 'integer|min:0',
            'buffer_after' => 'integer|min:0',
            'description' => 'string|min:6'
        ];
    }
}
