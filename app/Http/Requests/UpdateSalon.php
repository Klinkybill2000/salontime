<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSalon extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'profile_url' => 'required',
            'website' => 'url',
            'phone' => 'required',
            'zip' => 'required',
            'street' => 'required',
            'city' => 'required',
            'description' => 'string|min:3|max:160',
            'facebook' => 'url',
            'twitter' => 'url',
        ];
    }
}
