<?php

namespace App\Http\Controllers;

class CalendarController extends AppController
{
    /**
     * Display a calendar with salons's obligations
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.calendar.index', ['appointments' => $this->salon()->appointments]);
    }
}
