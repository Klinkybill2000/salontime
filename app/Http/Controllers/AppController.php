<?php

namespace App\Http\Controllers;

use Auth;

class AppController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Get the associated profile
     */
    public function salon(){
        return Auth::user()->userable;
    }
}
