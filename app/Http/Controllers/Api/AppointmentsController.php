<?php

namespace App\Http\Controllers\Api;

use App\{Salon, Customer, Appointment, Employee, Service};
use App\Notifications\BookingRequested;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentsController extends Controller
{
    /**
     * Store an appointment in the database
     *
     * @param Request $request
     * @return string
     */
    public function requestAppointment(Request $request, $salonId) {
        $attributes = $request->all();

        $salon = Salon::findOrFail($salonId);

        $customer = Customer::updateOrCreate([
            'email' => $attributes['customer']['email']],
            $attributes['customer']
        );

        $appointment = $salon->bookAppointment(
            array_merge($attributes['appointment'], ['customer_id' => $customer->id])
        );

        if ($appointment) {
            $customer->notify(new BookingRequested($appointment));
            $salon->notify(new BookingRequested($appointment));
        }

        if (request()->ajax()) {
            return response()->json(['status' => 'Your booking request has been sent!'], 201);
        }

        abort(403, 'Sorry, not sorry!');
    }

    /**
     * Get the available slots for the specefied employee and service
     *
     * @param Request $request
     * @param $service
     * @param $employee
     * @return mixed
     */
    public function timeslots(Request $request, $service, $employee) {
        $employee = Employee::findOrFail($employee);
        $service = Service::findOrFail($service);

        return $employee->timeslots($request->input('date'), $service->duration);
    }
}
