<?php

namespace App\Http\Controllers;

use App\{Employee, Service};
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateEmployee;

class EmployeesController extends AppController
{
    /**
     * Display a listing of the Employees
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.employees.index', ['employees' => $this->salon()->employees]);
    }

    /**
     * Store a newly created employee in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'phone' => 'string'
        ]);

        $this->salon()->hire(
            new Employee($request->only(['name', 'email', 'phone']))
        );

        return redirect()->back()->with('alert', 'Employee is stored successfully!');
    }

    /**
     * Update the specified employee in storage.
     *
     * @param UpdateEmployee $request
     * @param $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateEmployee $request, Employee $employee)
    {
        $this->authorize('employee-update', $employee);

        $employee->update($request->except('avatar'));

        if ($request->hasFile('avatar')) {
            $employee->proceedAvatar($request->file('avatar'));
        }

        return redirect()->back()->with('alert', 'Employee is edited successfully!');
    }

    /**
     * Display the specified employee.
     *
     * @param $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($employee)
    {
        $employee = Employee::with('services')->findOrFail($employee);
        $services = $this->salon()->services;
        $salon = $this->salon();

        $this->authorize('employee-view', $employee);

        return view('app.employees.profile.show', compact('employee', 'services', 'salon'));
    }

    /**
     * Edit the specefied working day of the salon
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editWorkingDay(Employee $employee, $day)
    {
        $employee->editWorkingHours($day, request()->all());

        if (request()->ajax()) {
            return response()->json(['status' => 'Working day plan is succesfully changed!'], 201);
        }

        abort(403, 'Sorry, not sorry!');
    }

    /**
     * Remove the specified employee from storage.
     *
     * @param $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Employee $employee)
    {
        $this->authorize('employee-delete', $employee);

        $employee->delete();

        return redirect()->back()->with('alert', 'Employee is deleted successfully!');
    }
}
