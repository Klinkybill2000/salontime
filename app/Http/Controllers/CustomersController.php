<?php

namespace App\Http\Controllers;

use App\Customer;
use Auth;
use Illuminate\Http\Request;

class CustomersController extends AppController
{
    /**
     * Display a listing of the Customers
     */
    public function index()
    {
        return view('app.customers.index', ['customers' => $this->salon()->customers]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'phone' => 'string',
            'note' => 'string'
        ]);

        $this->salon()->customers()->save(new Customer(request()->all()));

        return redirect()->back()->with('alert', 'Customer is added successfully!');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Customer $customer)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'phone' => 'string',
            'note' => 'string'
        ]);

        $this->authorize('customer-update', $customer);

        $customer->update($request->all());

        if (request()->ajax()) {
            return session()->flash('alert', 'Customer successfully updated!');
        }

        return redirect()->back()->with('alert', 'Customer successfully updated!');
    }

    /**
     * Remove the specified customer from storage.
     */
    public function destroy(Customer $customer)
    {
        $this->authorize('customer-delete', $customer);

        $customer->delete();

        return redirect()->back()->with('alert', 'Customer successfully deleted!');
    }
}
