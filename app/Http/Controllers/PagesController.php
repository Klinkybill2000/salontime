<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends AppController
{
    /**
     * Display landing page
     */
    public function landing()
    {
        return view('pages.landing');
    }

    /**
     * Display features page
     */
    public function features()
    {
        return view('pages.features');
    }

    /**
     * Display pricing page
     */
    public function pricing()
    {
        return view('pages.pricing');
    }

    /**
     * Display support page
     */
    public function support()
    {
        return view('pages.support');
    }

    /**
     * Display contact page
     */
    public function contact()
    {
        return view('pages.contact');
    }

    /**
     * Display impressions page
     */
    public function impressions()
    {
        return 'impressions';
    }

    /**
     * Display Impressum page
     */
    public function impressum()
    {
        return view('pages.impressum');
    }

    /**
     * Display AGB page
     */
    public function agb()
    {
        return view('pages.agb');
    }

    /**
     * Display Datenschutz page
     */
    public function privacy()
    {
        return view('pages.privacy');
    }
}
