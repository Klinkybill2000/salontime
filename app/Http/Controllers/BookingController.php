<?php

namespace App\Http\Controllers;

use App\Salon;

class BookingController extends Controller
{
    public function show($salon)
    {
        $salon = Salon::where('profile_url', $salon)->firstOrFail();

        return view('booking.show', compact('salon'));
    }
}
