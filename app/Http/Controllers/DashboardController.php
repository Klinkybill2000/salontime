<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends AppController
{
    /**
     * Show the Application Dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.dashboard');
    }
}
