<?php

namespace App\Policies;

use App\{User, Employee};
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the employee.
     *
     * @param  \App\User  $user
     * @param  \App\Employee  $employee
     * @return mixed
     */
    public function employeeView(User $user, Employee $employee)
    {
        return $user->salon->owns($employee);
    }

    /**
     * Determine whether the user can update the employee.
     *
     * @param  \App\User  $user
     * @param  \App\Employee  $employee
     * @return mixed
     */
    public function employeeUpdate(User $user, Employee $employee)
    {
        return $user->salon->owns($employee);
    }

    /**
     * Determine whether the user can delete the employee.
     *
     * @param  \App\User  $user
     * @param  \App\Employee  $employee
     * @return mixed
     */
    public function employeeDelete(User $user, Employee $employee)
    {
        return $user->salon->owns($employee);
    }
}
