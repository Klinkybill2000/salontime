<?php

namespace App\Policies;

use App\{User, Service, Employee};
use Illuminate\Auth\Access\HandlesAuthorization;

class ServicePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the service.
     *
     * @param  \App\User  $user
     * @param  \App\Service  $service
     * @return mixed
     */
    public function serviceView(User $user, Service $service)
    {
        return $user->salon->owns($service);
    }

    /**
     * Determine whether the user can update the service.
     *
     * @param  \App\User  $user
     * @param  \App\Service  $service
     * @return mixed
     */
    public function serviceUpdate(User $user, Service $service)
    {
        return $user->salon->owns($service);
    }

    /**
     * Determine whether the user can delete the service.
     *
     * @param  \App\User  $user
     * @param  \App\Service  $service
     * @return mixed
     */
    public function serviceDelete(User $user, Service $service)
    {
        return $user->salon->owns($service);
    }

    /**
     * Determine whether the user can attach/detach the service to an employee.
     *
     * @param  \App\User  $user
     * @param  \App\Service  $service
     * @return mixed
     */
    public function serviceToggle(User $user, Service $service, Employee $employee)
    {
        return $user->salon->owns($service) && $user->salon->owns($employee);
    }
}
