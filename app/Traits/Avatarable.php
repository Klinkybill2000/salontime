<?php

namespace App\Traits;

use Image;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;

trait Avatarable
{
    /**
     * Avatar's public path
     *
     * @var string
     */
    protected $publicPath = 'storage/avatars';

    /**
     * Avatar's base path
     *
     * @var string
     */
    protected $basePath = 'app/public/avatars';

    /**
     * Avatar's default width
     *
     * @var string
     */
    protected $baseWidth = 75;

    /**
     * Avatar's default height
     *
     * @var string
     */
    protected $baseHeight = 75;

    /**
     * Avatar's name
     *
     * @var string
     */
    protected $avatarName;


    /**
     * Get the full path of the avatar
     *
     * @param $value
     * @return string
     */
    public function getAvatarAttribute($value)
    {
        return sprintf('%s/%s', $this->publicPath, $value);
    }

    /**
     * Proceed the avatar to the database and folder
     *
     * @param UploadedFile $avatar
     * @return $this
     */
    public function proceedAvatar(UploadedFile $avatar)
    {
        $this->removeCurrentAvatar();

        return $this->generateAvatarName($avatar->getClientOriginalExtension())
            ->storeAvatar($avatar)
            ->saveAvatar();
    }

    /**
     * Prepare and store the image in the app's directory
     *
     * @param $avatar
     * @return $this
     */
    protected function storeAvatar($avatar)
    {
        $this->createAvatarsDirectory();

        Image::make($avatar)
            ->resize($this->baseWidth, $this->baseHeight)
            ->save(storage_path('app/public/avatars/') . $this->avatarName);

        return $this;
    }

    /**
     * Generate avatar's unique name
     *
     * @param $extension
     * @return $this
     */
    protected function generateAvatarName($extension)
    {
        $this->avatarName = sprintf('%d%d.%s', time(), uniqid(), $extension);

        return $this;
    }

    /**
     * Save the avatar into the database
     *
     * @return $this
     */
    protected function saveAvatar()
    {
        $this->avatar = $this->avatarName;

        $this->save();

        return $this;
    }

    /**
     * Remove the current avatar if exists
     *
     * @return mixed
     */
    protected function removeCurrentAvatar()
    {
        if (File::exists($this->avatar) && ! str_contains($this->avatar, 'default.png')) {
            return File::delete($this->avatar);
        }
    }

    /**
     * Create the avatars folder
     *
     * @return mixed
     */
    protected function createAvatarsDirectory()
    {
        if (! File::exists(storage_path($this->basePath))) {
            return File::makeDirectory(storage_path($this->basePath));
        }
    }
}
