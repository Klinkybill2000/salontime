<?php

namespace App;

use App\Traits\Avatarable;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use Avatarable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'phone', 'avatar'];

    /**
     * Get the Salongs that Employee belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    /**
     * Get the Services that belongs to the Employee
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    /**
     * Get appoinments that are booked at the specefied salon
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    /**
     * Get the Working plan that belongs to the Employee
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function workingDays()
    {
        return $this->morphMany('App\WorkingDay', 'workable');
    }

    /**
     * Get the specefied working day
     *
     * @param $day
     * @return mixed
     */
    public function workingDay($day)
    {
        return $this->workingDays()->where('weekday', $day)->first();
    }

    /**
     * Edit working hours of the specefied employee
     *
     * @param $plan
     * @param $day
     * @return mixed
     */
    public function editWorkingHours($day, $plan)
    {
        return $this->workingDay($day)->update($plan);
    }

    /**
     * Set the initial working plan when creating an employee
     *
     * @return array
     */
    public function withInitialWorkingPlan()
    {
        return $this->salon->workingDays->map(function ($day) {
            return new WorkingDay($day->toArray());
        });
//
//        return $this->workingDays()->saveMany($this->salon->workingDays->map(function ($day) {
//            return new WorkingDay($day->toArray());
//        }));
    }

    /**
     * Check if the specefied employee providing provided service
     *
     * @param $providerId
     * @return bool
     */
    public function isProviding($providerId)
    {
        return !! $this->services->where('id', $providerId)->first();
    }

    /**
     * Get the available employee's timeslots for the specefied date and service
     *
     * @param $date
     * @param $serviceDuration
     * @return array
     */
    public function timeslots($date, $serviceDuration)
    {
        return Timeslots::generate(
            $this->workingDayByDate($date),
            $this->appointmentsByDate($date),
            $date,
            $serviceDuration
        );
    }

    /**
     * Get employee's working day hours by the specefied date
     *
     * @param $date
     * @return mixed
     */
    public function workingDayByDate($date)
    {
        $weekDay = date('N', strtotime($date));

        return $this->workingDays()
                    ->where('weekday', $weekDay != 7 ? $weekDay : 0)
                    ->first();
    }

    /**
     * Get employee's working day hours by the specefied date
     *
     * @param $date
     * @return mixed
     */
    public function appointmentsByDate($date)
    {
        return $this->appointments()->whereDate('start', date('Y-m-d', strtotime($date)))->get();
    }
}
