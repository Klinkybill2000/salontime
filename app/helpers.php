<?php

if (! function_exists('dayOfWeek')) {
    /**
     * Returns a day of the week
     *
     * @param $weekday
     * @param bool $short
     * @return mixed|string
     */
    function dayOfWeek($weekday, $short = false)
    {
        $days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

        if ($short) {
            return substr($days[$weekday], 0, 3);
        }

        return $days[$weekday];
    }
}

if (! function_exists('closestToNumber')) {
    /**
     * Get a closest number in array to a specefied number
     *
     * @param $search
     * @param array $numbers
     * @return mixed|null
     */
    function closestToNumber($search, Array $numbers)
    {
        $closest = null;

        foreach ($numbers as $number) {
            if ($closest === null || abs($search - $closest) > abs($number - $search)) {
                $closest = $number;
            }
        }

        return $closest;
    }
}

if (! function_exists('activeRouteAction')) {
    /**
     * Checks if the current route acction is equal to the nav route action and apply active class
     *
     * @param $action
     * @return bool|string
     */
    function isActiveRouteAction($action) {
        $controller = substr_replace($action, '', strpos($action, '@'));

        return str_contains(Route::currentRouteAction(), $controller) ? 'active' : false;
    }
}