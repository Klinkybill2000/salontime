<?php

namespace App\Mail;

use App\{Salon, Appointment};
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SalonBookingRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The profile instance.
     */
    public $salon;

    /**
     * The appointment instance.
     */
    public $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(salon $salon, Appointment $appointment)
    {
        $this->salon = $salon;
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->salon->email)
                    ->from('no-reply@salontime.de')
                    ->subject('Booking request')
                    ->view('emails.booking.request_to_salon');
    }
}
