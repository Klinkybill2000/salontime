<?php

namespace App\Mail;

use App\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerBookingDenial extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The appointment instance
     */
    public $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->appointment->customer->email)
                    ->from('no-reply@salontime.de')
                    ->subject('Booking denial')
                    ->view('emails.booking.denial_to_customer');
    }
}
