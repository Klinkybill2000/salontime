<?php

namespace App\Mail;

use App\{Customer, Appointment};
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerBookingRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The customer instance.
     */
    public $customer;

    /**
     * The appointment instance.
     */
    public $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, Appointment $appointment)
    {
        $this->customer = $customer;
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->customer->email)
                    ->from('no-reply@salontime.de')
                    ->subject('Booking request')
                    ->view('emails.booking.request_to_customer');
    }
}
