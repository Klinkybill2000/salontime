<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingDay extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['weekday', 'work_start', 'work_end'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['day', 'isWorkable'];

    /**
     * Get all of the owning workable models.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function workable()
    {
        return $this->morphTo();
    }

    /**
     * Get the working plans's day in readable format
     *
     * @return mixed|string
     */
    public function getDayAttribute()
    {
        return dayOfWeek($this->attributes['weekday'], true);
    }

    /**
     * Check if the specified day is workable.
     *
     * @return bool
     */
    public function getIsWorkableAttribute()
    {
        return $this->attributes['work_start'] == '00:00:00'
        ||
        $this->attributes['work_end'] == '00:00:00' ? false : true;
    }

    /**
     * Get human readable opening times.
     *
     * @return string
     */
    public function forHumans()
    {
        return sprintf(
            "%s. %s - %s",
            $this->day,
            date('H:i', strtotime($this->work_start)),
            date('H:i', strtotime($this->work_end))
        );
    }
}
