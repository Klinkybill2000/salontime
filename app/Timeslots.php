<?php

namespace app;

use DateTime;

class Timeslots
{
    /**
     * All reserved time slots
     */
    protected $reserved;

    /**
     * The start of working day
     */
    protected $start_time;

    /**
     * The end of working day
     */
    protected $end_time;

    /**
     * Duration of the specefied service
     */
    protected $duration;

    /**
     * The list of available timeslots
     */
    protected $timeslots = [];

    /**
     * Timeslots constructor.
     *
     * @param $workingDay
     * @param $reserved
     * @param $date
     */
    public function __construct($workingDay, $reserved, $date)
    {
        $this->reserved = $reserved;

        $start_time = date('Y-m-d H:i:s', strtotime($date . ' ' . $workingDay->work_start));
        $end_time = date('Y-m-d H:i:s', strtotime($date . ' ' . $workingDay->work_end));

        $this->start_time = $this->createFromFormat($start_time);
        $this->end_time = $this->createFromFormat($end_time);
    }

    /**
     * Generate the timeslots for the specifed resource
     *
     * @param $workingDay
     * @param $appointments
     * @param $date
     * @param $duration
     * @return mixed
     */
    public static function generate($workingDay, $appointments, $date, $duration)
    {
        $self = new static($workingDay, $appointments, $date);

        return $self->getAvailableTimeSlots($duration);
    }

    /**
     * Get all available time slots for the specefied employee
     *
     * @param $duration
     * @return array
     */
    public function getAvailableTimeSlots($duration)
    {
        return $this->timeslots()
             ->cleanReserved()
             ->adaptForService($duration)
             ->get();
    }

    /**
     * Generate a solid time slots without any break, appointment or vacation
     *
     * @return $this
     */
    public function timeslots()
    {
        for ($i = clone $this->start_time; $i < clone $this->end_time;) {
            $slots = [
                'start' => $i->format('Y-m-d H:i:s'),
                'end' => $i->modify("+15 minutes")->format('Y-m-d H:i:s')
            ];

            if ($i <= $this->end_time) {
                array_push($this->timeslots, $slots);
            }
        }

        return $this;
    }

    /**
     * Remove reserved time slots from the list of the available timeslots
     *
     * @return $this
     */
    public function cleanReserved()
    {
        foreach ($this->timeslots as $index => $slot) {
            $t1 = strtotime($slot['start']);
            $t2 = strtotime($slot['end']);

            foreach ($this->reserved as $day) {
                $st = $this->createFromFormat($day->start);
                $en = $this->createFromFormat($day->end);

                if ((date_timestamp_get($st) > $t1 && date_timestamp_get($st) < $t2)
                    ||
                    (date_timestamp_get($en) > $t1 && date_timestamp_get($en) < $t2)
                    ) {
                    unset($this->timeslots[$index]);
                }

                if ($t1 >= date_timestamp_get($st) && $t2 <= date_timestamp_get($en)) {
                    unset($this->timeslots[$index]);
                }
            }
        }

        return $this;
    }

    /**
     * Adapt time slots for the service duration
     *
     * @param $duration
     * @return $this
     */
    public function adaptForService($duration)
    {
        foreach ($this->timeslots as $index => $slot) {
            $t1 = strtotime($slot['start']);
            $endTime = $this->end_time->getTimestamp();

            $closestAppointment = closestToNumber($t1, $this->reservedTimestamps());
            
            $diff = (int) round(abs($closestAppointment - $t1) / 60);
            $openingTimeDiff = (int) round(abs($t1 - $this->start_time->getTimestamp()) / 60);

            if ($diff < $duration && $openingTimeDiff >= $duration) {
                unset($this->timeslots[$index]);
            }

            $diff = (int) round(abs($t1 - $endTime) / 60);

            if ($diff < $duration) {
                unset($this->timeslots[$index]);
            }
        }

        return $this;
    }

    /**
     * Timestamp List of Reserved timeslots
     *
     * @return array
     */
    public function reservedTimestamps()
    {
        return array_map(function ($day) {
            return strtotime($day['start']);
        }, $this->reserved->toArray());
    }

    /**
     * Fetch available timeslots
     *
     * @return array
     */
    public function get()
    {
        return array_column($this->timeslots, 'start');
    }

    /**
     * Create DateTime object from format
     *
     * @param $time
     * @return bool|DateTime
     */
    public function createFromFormat($time)
    {
        return DateTime::createFromFormat('Y-m-d H:i:s', $time);
    }
}
