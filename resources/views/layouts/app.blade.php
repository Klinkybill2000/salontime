<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name') }}</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">

    <!-- Styles -->
    <link href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/plugins/nvd3/nv.d3.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/plugins/mapplic/css/mapplic.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('pages/css/pages-icons.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/datatables-responsive/css/datatables.responsive.css') }}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{ asset('assets/plugins/dropzone/css/dropzone.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/tapatar.min.css') }}" rel="stylesheet">

    <link class="main-stylesheet" href="{{ asset('pages/css/pages.css') }}" rel="stylesheet">
    @yield('links')
    <!--[if lte IE 9]>
    <link href="{{ asset('assets/plugins/codrops-dialogFx/dialog.ie.css') }}" rel="stylesheet" type="text/css" media="screen">
    <![endif]-->

    <!-- <link href="/css/app.css" rel="stylesheet"> -->
    
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
  </head>
  <body class="fixed-header dashboard">
    <!-- Application Wrapper -->
    <div id="app">
      <!-- BEGIN SIDEBAR-->
      @include('app.partials._sidebar')

      <!-- START PAGE-CONTAINER -->
      <div class="page-container ">

        <!-- START HEADER -->
        @include('app.partials._header')

        <!-- START PAGE CONTENT WRAPPER -->
        <div class="page-content-wrapper">
          <!-- START PAGE CONTENT -->
          <div class="content sm-gutter">
            <!-- START CONTAINER FLUID -->
            @yield('content')
            <!-- END CONTAINER FLUID -->
            
            <alert @if (session('alert')) flash="{{ session('alert') }}" @endif></alert>
          </div>
          <!-- END PAGE CONTENT -->

          <!-- Footer -->
          @include('app.partials._footer')
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
      </div>
      <!-- END PAGE CONTAINER -->
      
      <!-- START OVERLAY -->
      @include ('app.partials._overlay_search')
    </div>
    
    <!-- Vendors -->
    <script src="{{ asset('js/vendor.js') }}"></script>

    <!-- BEGIN CORE TEMPLATE JS -->
    
    <!-- Main JS -->
    <script src="{{ asset('js/app.js') }}"></script>
    
    <script src="{{ asset('js/pages.js') }}"></script>

    <!-- ADDITIONAL SCRIPTS -->
    @yield('scripts')
  </body>
</html>