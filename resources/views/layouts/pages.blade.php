<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>

    <title>Salontime – Deine Salonverwaltung</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN PLUGINS -->
    <link href="assets_frontend/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
    <link href="assets_frontend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets_frontend/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="assets_frontend/plugins/swiper/css/swiper.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- END PLUGINS -->
    <!-- BEGIN PAGES CSS -->
    <link class="main-stylesheet" href="pages_frontend/css/pages.css" rel="stylesheet" type="text/css"/>
    <link class="main-stylesheet" href="pages_frontend/css/pages-icons.css" rel="stylesheet" type="text/css"/>
    <!-- BEGIN PAGES CSS -->
</head>
<body class="pace-dark">

<!-- BEGIN HEADER -->
@include('pages.partials._header')
<!-- END HEADER -->

@yield('content')

<!-- BEGIN FOOTER -->
@include('pages.partials._footer')
<!-- END FOOTER -->

<!-- START OVERLAY SEARCH -->
@include('pages.partials._overlay_search')
<!-- END OVERLAY SEARCH -->

<!-- BEGIN CORE FRAMEWORK -->
<script src="assets_frontend/plugins/pace/pace.min.js"></script>
<script src="pages_frontend/js/pages.image.loader.js"></script>
<script src="assets_frontend/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="assets_frontend/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- BEGIN SWIPER DEPENDENCIES -->

@yield('scripts')
</body>
</html>