<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"
    >
    <meta charset="utf-8"/>

    <title>{{ $salon->name }} Salon</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">

    <link rel="icon" type="image/x-icon" href="favicon.ico"/>

    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">

    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('pages/css/pages-icons.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">

    <link class="main-stylesheet" href="{{ asset('pages/css/pages.css') }}" rel="stylesheet">
    <link class="main-stylesheet" href="{{ asset('pages/css/booking/widget.css') }}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{ asset('pages/css/booking/style.css') }}" rel="stylesheet" type="text/css">

    <style>
        #map {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 100%;
        }
    </style>

    <!--[if lte IE 9]>
    <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css">
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = <?= json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="fixed-header menu-pin menu-behind ">
<!-- START PAGE-CONTAINER -->
<div class="page-container" id="app">
    <!-- START HEADER -->
    <div class="header bg-info">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
            <!-- LEFT SIDE -->
            <div class="pull-left full-height visible-sm visible-xs bg-info">
                <!-- END ACTION BAR -->
            </div>
            <div class="pull-center hidden-md hidden-lg bg-info">
                <div class="header-inner">
                    <div class="brand inline">
                        <img src="{{ asset('assets/img/logo_white.png') }}" alt="logo"
                             data-src="{{ asset('assets/img/logo_white.png') }}"
                             data-src-retina="{{ asset('assets/img/logo_white_2x.png') }}" width="78" height="12">
                    </div>
                </div>
            </div>
            <!-- RIGHT SIDE -->
            <div class="pull-right full-height visible-sm visible-xs ">
                <!-- START ACTION BAR -->
                <div class="header-inner">
                    <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview"
                       data-toggle-element="#quickview"><span class="icon-set menu-hambuger-plus"></span></a>
                </div>
                <!-- END ACTION BAR -->
            </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm">
            <div class="header-inner">
                <div class="brand inline">
                    <a href="{{ action('PagesController@landing') }}">
                        <img src="{{ asset('assets/img/logo_white.png') }}" alt="logo"
                             data-src="{{ asset('assets/img/logo_white.png') }}"
                             data-src-retina="{{ asset('assets/img/logo_white_2x.png') }}" width="152" height="21">
                    </a>
                </div>
            </div>
        </div>
        <div class=" pull-right">
            <div class="header-inner">
            </div>
        </div>
        <div class=" pull-right">
        </div>
    </div>
    <!-- END HEADER -->

    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">


        <!-- START PAGE CONTENT -->
        <div class="content contentcenter">
            <!-- START CONTAINER Left -->

            <!-- START MAIN SECTION -->
            <div class="col-md-8 col-xs-12">
                <div class="m-t-50" style="box-shadow: 10px 20px 30px grey;">
                    <div class=" no-padding">
                        <div class="">
                            <div class="col-md-6 no-padding">
                                <div class="bg-success-lighter" style="min-height: 220px; margin-bottom: 0px;">
                                    <div id='map'></div>

                                </div>
                            </div>
                            <div class="col-md-6 no-padding">
                                <div class="bg-white" style="min-height: 220px; margin-bottom: 0px;">
                                    <div class="panel-heading">
                                        <div class="panel-title col-md-6 p-t-10">
                                        </div>
                                        <img src="{{ asset('assets/img/wm-logo.svg') }}" alt="logo"
                                             data-src="{{ asset('assets/img/wm-logo.svg') }}"
                                             data-src-retina="{{ asset('assets/img/wm-logo.svg') }}">

                                    </div>
                                    <div class="panel-body no-padding p-l-20 p-r-20">
                                        <h3>
                                            <span class="semi-bold">Welcome at</span> our Salon</h3>

                                        <p>{{ $salon->description }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 no-padding">
                                <div class="panel bg-info text-white"
                                     style="min-height: 220px; margin-bottom: 0px; text-align: center;">
                                    <div class="panel-heading m-t-30">
                                        <div class="panel-title "><h4>
                                                <span class="semi-bold text-white">{{ $salon->name }}</span></h4>
                                        </div>

                                    </div>
                                    <div class="panel-body" style="max-height:150px; text-align: center; ">
                                        <span class="font-montserrat fs-18 all-caps">{{ $salon->city }}</span><br>
                                        <span class="font-montserrat fs-18 all-caps">{{ $salon->zip }} {{ $salon->street }}</span><br>
                                        <span class="font-montserrat fs-18 all-caps">Tel. {{ $salon->phone }}</span><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 no-padding">
                                <div class="panel bg-info text-white" style="min-height: 220px; margin-bottom: 0px;">
                                    <div class="panel-heading" style="text-align: center;">
                                        <div class="panel-title">Opening times:
                                        </div>

                                    </div>
                                    <div class="panel-body" style="text-align: center">
                                        <p>
                                            @foreach ($salon->workingDays as $day)
                                                @if ($day->isWorkable)
                                                    <span class="all-caps">{{ $day->forHumans() }}</span><br>
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizard-footer padding-20 bg-master-light">


                        <div class="clearfix"></div>
                        <div class="padding-20 p-t-20 bg-white">
                            <ul class="pager wizard">
                                <li class="save">
                                    <button class="btn btn-primary btn-lg" data-toggle="modal"
                                            data-target="#modalSlideUp"> Book an appointment
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <p class="small hint-text pull-left no-margin">
                        <div class="pull-right p-t-20">
                            <img src="{{ asset('assets/img/logo.png') }}" alt="logo"
                                 data-src="{{ asset('assets/img/logo.png') }}"
                                 data-src-retina="{{ asset('assets/img/logo_2x.png') }}" width="76" height="11">
                        </div>
                        <div class="p-t-20">
                            <p>
                                Salontime bietet Dir eine kinderleichte Online Terminplanung. Buche direkt
                                Deinen nächsten Termin.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN SECTION -->
            <!-- END CONTAINER Left -->

        </div>

        <alert></alert>
    </div>
    <!-- START Modal -->
    <book-appointment
            :salon-id="{{ $salon->id }}"
            :services="{{ $salon->services }}"
    ></book-appointment>
</div>
<!-- END Page Content -->

<!-- End Modal -->
<div class="col-md-7">
    <div class=" sm-text-center bg-white">
        <p class="small no-margin pull-left sm-pull-reset"><span class="hint-text">Copyright &copy; 2016 </span><span
                    class="font-montserrat">Salontime</span>.<span class="hint-text">All rights reserved. </span><span
                    class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
        </p>
        <p class="small no-margin pull-right sm-pull-reset">
        <div class="m-t-20 m-b-20"></div>
    </div>
</div>

<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/pages.js') }}"></script>

<script>
    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 6,
            mapTypeControl: false,
            streetViewControl: false,
            zoomControl: false,
            scaleControl: false,
            styles: [{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{"color": "#dedede"}, {"lightness": 21}]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
            }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
            }
            ]
        });
    }
    ;
</script>
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5ZTffuqBrZrGoDKPwLAwLwf6nNbOeD2o&callback=initMap"
        async defer
></script>
</body>
