<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!--<![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title></title>
  <!-- Change the title -->

  <style type="text/css">
  .ReadMsgBody { width: 100%; background-color: #ffffff; }
  .ExternalClass { width: 100%; background-color: #ffffff; }
  .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
  html { width: 100%; }
  body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
  table { border-spacing: 0; table-layout: fixed; margin: 0 auto; }
  table table table { table-layout: auto; }
  .yshortcuts a { border-bottom: none !important; }
  img:hover { opacity: 0.9 !important; }
  a { color: #f69679; text-decoration: none; }

  span.preheader{ display: none; font-size: 1px; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0; } 

  @media only screen and (max-width: 640px) {
    body { width: auto !important; } 
    img[class="full"] { width: 100% !important; height: auto !important; }
  }

  @media only screen and (max-width: 480px) {
    body { width: auto !important; }
    img[class="full"] { width: 100% !important; }
  }
  </style>
</head>

<body>

  <!-- Pre Header -->    
  <!-- Change this to text to your own, this text will preview in mail client as excerpt. -->
  <span class="preheader" style="display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;border-collapse: collapse;border: 0px;">
      You forgot your password? Change it here 
  </span>
  <!-- End of Pre Header -->


  <!-- Outer 50Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="50"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Outer 50Space -->

  <!-- Body 35Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#2b303b" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
               
                <tr>
                  <td height="35"></td>
                </tr>
                  
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Body 35Space -->  

  <!-- Logo-Title -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#2b303b" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="15"></td>
                </tr>
                
                <tr>
                  <td align="center" style="line-height: 0px;">

                    <!-- edit logo -->
                    <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="{{ asset('images/email/logo.png') }}" alt="img" />

                  </td>
                </tr>
               
                <tr>
                  <td height="40"></td>
                </tr>

                <tr>
                  <td align="center" style="font-family: 'Montserrat', sans-serif; font-size: 20px; font-weight: 700; color: #FFFFFF; letter-spacing: 2px; line-height: 24px;">
                  
                    <!-- Edit Title -->
                    RESET YOUR PASSWORD!
                  
                  </td>
                </tr>

                <!-- underline -->
                <tr>
                  <td align="center">
                    <table align="center" width="50" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <!-- edit color -->
                        <td height="20" style="border-bottom:3px solid #10cfbd;"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <!-- end underline -->

                <tr>
                  <td height="15"></td>
                </tr>

              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Logo-Title -->

  <!-- Body 35Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#2b303b" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
               
                <tr>
                  <td height="35"></td>
                </tr>
                  
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Body 35Space -->  

  <!-- BG-Image -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center" bgcolor="#202020" background="{{ asset('images/header-bg.png') }}" style="background-size:100% auto; background-repeat:repeat-x;">
                    <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="70"></td>
                      </tr>

                      <tr>
                        <td width="500" align="center">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">

                            <tr>
                              <td align="center" style="font-family: 'Montserrat', sans-serif; font-size: 15px; font-weight: 900; color: #ffffff; letter-spacing: 1px; line-height: 24px;">
                              
                                <!-- Edit Title -->
                                HALLO, DUSAN SARAC
                              
                              </td>
                            </tr>

                            <tr>
                              <td height="30"></td>
                            </tr>

                            <tr>
                              <td align="center" style="line-height: 0px;">

                                <!-- edit image -->
                                <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="{{ asset('images/email/key.png') }}" alt="img" />

                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>

                      <tr>
                        <td height="70"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of BG-Image -->

  <!-- Body 35Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#FFFFFF" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
               
                <tr>
                  <td height="35"></td>
                </tr>
                  
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Body 35Space --> 

  <!-- Text-Area -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#FFFFFF" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center">
                    <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
  
                      <tr>
                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;"> 
                        
                          <!-- Edit Content -->
                          You forgot your password? No problem, you can easily reset it here in few seconds.

                        </td>
                      </tr>

                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Text-Area -->  

  <!-- Icon-Button -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#FFFFFF" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center">
                    <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="30"></td>
                      </tr>

                      <tr>
                        <td align="center">
                          <table bgcolor="#272b35" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td align="left" height="38" width="20" style="padding-left: 25px;">
                                <img src="{{ asset('images/email/password-icon.png') }}" alt="icon" width="15" height="15" />
                              </td>
                              <td height="35" align="left" style="font-family: 'Montserrat', sans-serif; font-size:12px; font-weight: 400; color:#ffffff; padding-right:25px;">

                                <!-- edit link -->
                                <a href="{{ action('Auth\ResetPasswordController@showResetForm', $token) }}" style="color:#ffffff; text-decoration: none;">

                                  <!-- edit text -->
                                  &nbsp;RESET PASSWORD

                                </a>


                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>

                      <tr>
                        <td height="30"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Icon-Button --> 

  <!-- 2-Text-Links -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#FFFFFF" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center">
                    <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
  
                      <tr>
                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;"> 
                        
                          <!-- Change to your custom URL -->
                          <a href="http://www.salontime.de/agb" style="color:#999999; text-decoration: none;"> 
                          
                            <!-- Edit Link Text -->
                            AGB
                            
                          </a>  

                          <span>&nbsp;&nbsp;/&nbsp;&nbsp;</span>        
                            
                          <!-- Change to your custom URL -->      
                          <a href="http://www.salontime.de/privacy" style="color:#999999; text-decoration: none;"> 
                            
                          <!-- Edit Link Text -->         
                            Datenschutz  
                            
                          </a>  

                        </td>
                      </tr>

                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of 2-Text-Links -->

  <!-- Body 50Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table bgcolor="#FFFFFF" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
               
                <tr>
                  <td height="50"></td>
                </tr>
                  
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Body 50Space -->

  <!-- Outer 15Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Outer 15Space -->   

  <!-- Email-Option -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center" >
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="5"></td>
                </tr>
  
                <tr>
                  <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 11px; font-weight: 400; color: #7f8c8d;">

                    

                    

                    <!-- Change to your custom URL -->
                    <a href="http://www.salontime.de/unsubscribe" style="color:#7f8c8d; text-decoration: none;">

                      <!-- Edit Button Text -->
                      unsubscribe

                    </a>

                  </td>
                </tr>

                <tr>
                  <td height="5"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Email-Option -->

  <!-- Outer 15Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Outer 15Space -->   

  <!-- Footer-Content -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="500" align="center">
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="5"></td>
                </tr>
     
                <tr>
                  <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 11px; font-weight: 400; color: #7f8c8d; line-height: 24px;">

                    <!-- Edit Text -->
                    You are receiving this email because you opted in on our website or when you purchased our products.

                  </td>
                </tr>

                <tr>
                  <td height="5"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Footer-Content -->

  <!-- Outer 50Space -->
  <table align="center" bgcolor="#e8eaf0" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="50"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- End of Outer 50Space --> 

</body>

</html>
