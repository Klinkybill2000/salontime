@extends ('layouts.pages')

@section('content')
    <section class="p-t-100 sm-p-t-30 p-b-30 bg-master-lighter">
        <!-- BEGIN PRICING SECTION -->
        <section class="p-t-40 p-b-75">
            <div class="container p-t-10">
                <h1 class="text-center ">AGB (Allgemeine Geschäftsbedingungen)<br></h1>
                <div class="col-md-2"></div>
                <div class="col-md-8 center m-t-20">
                    Die nachfolgenden Regelungen stellen die zwischen Salontime, Andreas Klingenberger, Frankenring 84,
                    41812 Erkelenz (weiterhin „Salontime“, „wir“, „uns“ oder „unser“) und Ihnen geltenden Allgemeinen
                    Geschäftsbedingungen („AGB“) dar.<br><br>

                    Bitte lesen Sie diese sorgfältig durch, bevor Sie mit der Nutzung unserer Seite fortfahren.<br><br>

                    Bitte beachten Sie, dass diese AGB nicht die rechtliche Beziehung zwischen Salontime und unseren
                    Partnern regeln. Hierfür gelten unsere Vertragsbestimmungen für Partner.<br><br>

                    Unsere AGB enthalten unter anderem Regelungen zu folgenden Themen, welche Sie durch Anklicken dieser
                    Links unmittelbar aufrufen können:<br><br>
                    <h5>1. Vertragsgegenstand</h5>

                    Wir betreiben unter www.Salontime.de und weiteren Domains ein Onlineportal, welches auch über
                    Drittseiten, soziale Netzwerke, über andere Plattformen sowie über mobile Apps (iPhone, Android,
                    Windows Phone etc) erreichbar ist. Dieses Onlineangebot wird im Folgenden zusammenfassend als
                    „Salontime.de“ bezeichnet.
                    Salontime.de dient dem Auffinden und Bewerten von Dienstleistern im Friseur-, Kosmetik- und
                    Beautybereich. Teilnehmende Dienstleister („Partner“) aus diesen Bereichen veröffentlichen, damit
                    Sie sich informieren können, jeweils ein Profil auf Salontime.de mit einer Beschreibung der
                    angebotenen Leistungen.
                    Darüber hinaus bietet Ihnen Salontime die Möglichkeit, Termine bei teilnehmenden Partnern direkt
                    über das Internet zu buchen. Angeboten werden zur Zeit die Erfassung, Bearbeitung und Weiterleitung
                    von Terminanfragen und die direkte Buchung eines Termins bei dem Partner.
                    Die Nutzung von Salontime.de ist für Sie kostenlos.<br><br>
                    <h5>2. Vertragsparteien und Geltungsbereich</h5>

                    Diese AGB gelten für jede Nutzung von Salontime.de, unabhängig von der aufgerufenen Domain oder
                    Subdomain oder der gewählten Zugangsart, und für die über Salontime.de vermittelten Verträge
                    zwischen Ihnen und den Partnern.
                    Die Nutzung von Salontime.de und angebotenen Diensten ist nur natürlichen Personen gestattet, welche
                    das 18.Lebensjahr vollendet haben.
                    Der Vertrag über die Nutzung von Salontime.de kommt mit Salontime zustande. In Bezug auf die von den
                    Partnern angebotenen Leistungen tritt Salontime jedoch nur als reine Vermittlerin auf und ist durch
                    die Partner bevollmächtigt worden, als deren Vertreter aufzutreten.
                    Wenn Sie an Rabatt- oder Promotions- Aktionen auf unserer Webseite teilnehmen, können dafür
                    gesonderte vertragliche Bestimmungen zusätzlich zu den AGB gelten. Bei Abweichungen von den AGB
                    gehen diese gesonderten Regelungen vor.<br><br>
                    <h5> 3. Vertragsschluss bei Buchungen</h5>

                    Sie können mit Hilfe von Salontime.de Dienstleistungen unserer Partner buchen. Eine Buchung ist das
                    durch Sie gegenüber dem jeweiligen Partner abgegebene Angebot zum Erwerb einer Leistung unter
                    Nutzung von Salontime.de (wie nachfolgend unter Ziffer 4.2 im Detail beschrieben). Bei Annahme der
                    Buchung schließen Sie nur mit dem Partner einen Vertrag über die Leistung, nicht aber mit uns.
                    Salontime ist in diesem Fall nur Vertreter des jeweiligen Partners im Rahmen des Vertragsschlusses
                    und vermittelt in dieser Eigenschaft den Vertragsschluss zwischen Ihnen und dem jeweiligen Partner.
                    Die Buchung einer Leistung bei einem Partner läuft wie folgt ab:<br>

                    Nach der Auswahl des Partners auf der Webseite werden Sie auf eine Seite geleitet, auf welcher Sie
                    die konkrete gewünschte Leistung (den Service), einen bestimmten Mitarbeiter des Partners (wenn
                    unterschiedliche Mitarbeiter angezeigt werden) und einen gewünschten Termin auswählen können
                    Sie haben dann die Möglichkeit, eine Anrede, Ihren Vornamen und Ihren Nachnamen, eine Telefonnummer
                    und eine E-Mail-Adresse anzugeben, unter welcher Sie erreicht werden können.
                    Sie haben sodann gegebenenfalls die Möglichkeit, sich für eine Zahlungsoption zu entscheiden. Sie
                    können zwischen Onlinezahlung und der Zahlung vor Ort wählen. Weitere Informationen dazu finden Sie
                    bei den Zahlungsbedingungen unter Ziffer 6 dieser AGB.
                    Sie haben des Weiteren die Möglichkeit, etwaige Geschenkgutschein- oder Rabattcodes einzugeben.
                    In einem weiteren Schritt können Sie bestätigen, dass Sie die AGB und Datenschutzerklärung von
                    Salontime, sowie die Widerrufsbelehrung zur Kenntnis genommen haben und akzeptieren.
                    In einem letzten Schritt können Sie durch Anklicken des eindeutig hervorgehobenen Buchungsbuttons
                    die Buchung verbindlich absenden.<br>

                    Salontime kann die Verfügbarkeit des Partners zum gebuchten Termin nicht garantieren. Nachdem Sie
                    Ihre Buchung abgesendet haben, prüft Salontime deshalb die Verfügbarkeit des Partners für den
                    gebuchten Termin. Je nach Verfügbarkeit des Partners für den gewählten Termin kann der
                    Vertragsschluss zwischen Ihnen und dem Partner auf zwei Arten erfolgen:<br>

                    Bei Verfügbarkeit des Partners für den gewählten Termin nimmt Salontime für den Partner die Buchung
                    an. Der Vertrag über die Leistung zwischen Ihnen und dem Partner kommt somit erst zustande, wenn wir
                    Ihre Buchung für den Partner angenommen haben. In diesen Fall bekommen Sie von uns eine Email an die
                    von Ihnen während des Buchungsprozesses oder bei Ihrer Registrierung auf unserer Webseite angegebene
                    E-Mail-Adresse, in welcher wir für den Partner die Annahme Ihrer Buchung erklären.
                    Sollte ein Partner für den gebuchten Termin nicht verfügbar sein, kann Ihnen Salontime im Namen des
                    Partners alternative Terminvorschläge machen, wovon Sie einen annehmen können. Salontime wird Ihnen
                    die alternativen Termine in einer Email mitteilen. Mit Ihrer Annahme eines dieser vorgeschlagenen
                    Termine kommt ein Vertrag zwischen Ihnen und dem Partner zustande.<br>

                    Während des Buchungsprozesses können Sie Ihre Angaben auf Fehler überprüfen und diese ggfs.
                    korrigieren. Bitte nehmen Sie sich hierfür die Zeit, bevor Sie Ihre Buchung verbindlich absenden<br><br>
                    <h5>4. Vertragsschluss beim Kauf von Salontime Geschenkgutscheinen</h5>

                    Salontime Geschenkgutscheine können von Ihnen verwendet werden, um Produkte von Partnern zu
                    bezahlen, jedoch nur, wenn die Online-Zahlung möglich ist.
                    Sämtliche Salontime Geschenkgutschein sind nur für einen bestimmten, auf dem Salontime
                    Geschenkgutschein ausgewiesenen Zeitraum gültig („Gültigkeitszeitraum“). Der Gültigkeitszeitraum
                    beträgt zwölf Monate ab Buchung des Salontime Geschenkgutschein. Der Salontime Geschenkgutschein
                    kann nach Ablauf des Gültigkeitszeitraum nicht mehr eingelöst werden.
                    Wenn Sie einen Gutschein kaufen wollen, verläuft der Kauf mit uns folgendermaßen:

                    Nach der Auswahl eines bestimmten Geschenkgutscheins auf der Webseite werden Sie auf eine Seite
                    geleitet, auf welcher Sie den gewünschten Salontime Geschenkgutschein erwerben können.
                    Sie haben dann die Möglichkeit, eine Anrede, Ihren Vornamen und Ihren Nachnamen und eine
                    E-Mail-Adresse anzugeben, unter welcher Sie erreichbar sind
                    In einem weiteren Schritt können Sie bestätigen, dass Sie die AGB sowie die Datenschutzerklärung von
                    Salontime gelesen haben und akzeptieren. Außerdem können Sie hier bestätigen, dass sie über das
                    Ihnen ggf. zustehende Widerrufsrecht unterrichtet wurden und dieses zur Kenntnis genommen haben.
                    Sie haben sodann die Möglichkeit, die Zahlung des Salontime Geschenkgutscheins über unser
                    Zahlungssystem (siehe Ziffer 7) vorzunehmen („Jetzt bezahlen“). Hier machen Sie die für die
                    ausgewählte Art der Zahlung notwendigen Angaben.
                    In einem letzten Schritt können Sie durch Anklicken des eindeutig hervorgehobenen Kaufbuttons den
                    Kauf abschließen.

                    Während des Kaufprozesses können Sie Ihre Angaben auf Fehler überprüfen und diese ggfs. korrigieren.
                    Bitte nehmen Sie sich hierfür die Zeit, bevor Sie Ihren Kauf verbindlich abschließen.
                    Nach dem Absenden des Kaufangebots werden wir Ihnen eine Empfangsbestätigungsemail senden. Mit
                    Versendung dieser E-Mail nehmen wir Ihr Angebot auf Kauf eines Geschenkgutscheins ausdrücklich an
                    und der Vertrag mit Ihnen kommt zustande (Vertragsschluss). In dieser E-Mail ist auch der
                    Gutscheincode enthalten, den Sie bei einer Buchung über die Website eingeben und damit im Voraus
                    eine Leistung eines Partners bezahlen können.<br><br>
                    <h5>5. Widerrufsrecht</h5>

                    Nutzer, die Verbraucher im Sinne von § 13 BGB sind (jede natürliche Person, die den Vertrag zu einem
                    Zweck abschließt, der weder ihrer gewerblichen noch ihrer selbständigen beruflichen Tätigkeit
                    zugerechnet werden kann) und welche mit Salontime einen Vertrag über den Kauf eines
                    Geschenkgutscheins oder mit einem Partner einen Vertrag über die Erbringungen von Dienstleistungen
                    abschließen, haben das nachfolgende Widerrufsrecht. Salontime ist von jedem Partner bevollmächtigt
                    eine etwaige Widerrufserklärung als Vertreter des Partners in Empfang zu nehmen.
                    – Widerrufsbelehrung –
                    <br>
                    Widerrufsrecht: Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu
                    widerrufen. Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag des Vertragsabschlusses. Um Ihr
                    Widerrufsrecht auszuüben, müssen Sie uns (Salontime DACH GmbH, Am Beirr 6, 41839 Wassenberg, Tel.
                    0174 2100569, Email: info@Salontime.de) mittels einer eindeutigen Erklärung (z. B. ein mit der Post
                    versandter Brief, Telefax oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen,
                    informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht
                    vorgeschrieben ist. Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die
                    Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.
                    <br>
                    Folgen des Widerrufs Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von
                    Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die
                    sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste
                    Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag
                    zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist.
                    Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen
                    Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in
                    keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. Haben Sie verlangt, dass die
                    Dienstleistungen während der Widerrufsfrist beginnen soll, so haben Sie uns einen angemessenen
                    Betrag zu zahlen, der dem Anteil der bis zu dem Zeitpunkt, zu dem Sie uns von der Ausübung des
                    Widerrufsrechts hinsichtlich dieses Vertrags unterrichten, bereits erbrachten Dienstleistungen im
                    Vergleich zum Gesamtumfang der im Vertrag vorgesehenen Dienstleistungen entspricht.
                    <br>
                    Besondere Hinweise Das Widerrufsrecht erlischt bei einem Vertrag zur Erbringung von
                    Dienstleistungen, wenn der Unternehmer die Dienstleistung vollständig erbracht hat und mit der
                    Ausführung der Dienstleistung erst begonnen hat, nachdem der Verbraucher dazu seine ausdrückliche
                    Zustimmung gegeben hat und gleichzeitig seine Kenntnis davon bestätigt hat, dass er sein
                    Widerrufsrecht bei vollständiger Vertragserfüllung durch den Unternehmer verliert.<br>
                    – Ende der Widerrufsbelehrung –<br><br>
                    Muster-Widerrufsformular<br><br>

                    „Wenn Sie den Vertrag widerrufen wollen, dann füllen Sie bitte dieses Formular aus und senden Sie es
                    zurück an:<br>

                    Salontime, Inh. Andreas Klingenberger, <br>Frankenring 84,<br> 41812 Erkelenz<br> oder
                    info@Salontime.de <br><br>Hiermit widerrufe(n) ich/wir* den von mir/uns* abgeschlossenen Vertrag
                    über den Kauf folgender Waren __________________________(*)/die Erbringung der folgenden
                    Dienstleistung __________________________(*): <br>Bestellt am __________________________(*)/erhalten
                    am __________________________(*): <br>Name des/der Verbraucher(s): __________________________<br>Anschrift
                    des/der Verbraucher(s): Unterschrift des/der Verbraucher(s) <br>(nur bei Mitteilung auf Papier):
                    __________________________ Datum: (*)<br> Unzutreffendes streichen.“<br><br>
                    <h5>6. Zahlungsbedingungen</h5>

                    Abhängig davon welchen Partner Sie ausgesucht haben, wird von uns die Möglichkeit angeboten, die in
                    Anspruch genommenen Leistungen vor Ort bar zu bezahlen und/oder bei der Buchung über Salontime.de
                    die gebuchten Leistungen im Voraus online zu bezahlen. Bei der Onlinezahlung nehmen wir
                    stellvertretend für den Partner die Zahlung an. Geschenkgutscheine können Sie nur online bezahlen.
                    Sofern Sie online bezahlen, müssen Zahlungen für gebuchte Leistungen oder gekaufte Gutscheine im
                    Rahmen des Buchungs- bzw. Kaufprozesses in Euro über unser Online-Zahlungssystem („Zahlungssystem“)
                    mit den angegebenen Zahlungsmethoden durchgeführt werden (z.B. PayPal oder Kreditkarte). Hierfür
                    müssen Sie die für die jeweils gewählte Bezahlmethode notwendigen Angaben machen.
                    Wir bemühen uns in angemessenem Umfang, unser Zahlungssystem verfügbar und funktionsbereit zu
                    halten. Wir können aber eine ununterbrochene vollständige Verfügbarkeit ebenso wenig garantieren wie
                    Fehlerfreiheit. Wir versuchen, ihre Zahlungen zügig zu bearbeiten, gleichwohl können wir Ihnen die
                    Einhaltung einer bestimmten Bearbeitungszeit wir nicht zusichern, da die Bearbeitungszeit auch von
                    beteiligten Banken und Karten-Netzwerken abhängt und wir auf diese keinen Einfluss haben. Jederzeit
                    kann unser Zahlungssystem nur eingeschränkt oder gar nicht verfügbar sein, zum Beispiel wegen
                    Instandsetzungsarbeiten oder Aktualisierungen. Vorhersehbare Beeinträchtigungen des Zahlungssystems
                    werden wir nach Möglichkeit ankündigen und so kurz wie möglich halten.
                    Wenn Sie unser Zahlungssystem nutzen wollen, sind Sie verpflichtet, für eine ausreichende Deckung
                    des betreffenden Kontos zu sorgen bzw. bei der Zahlung mit Kreditkarte Ihren jeweiligen
                    Verfügungsrahmen nicht zu überschreiten.
                    Für die Nutzung des Zahlungssystems erheben wir keine Gebühren von Ihnen.<br><br>
                    <h5>7. Mitgliedschaft</h5>

                    Wenn Sie Sich auf Salontime.de registrieren und ein Nutzerkonto anlegen können Sie mit der
                    Zustimmung zu diesen AGB und unserer Datenschutzerklärung registriertes Mitglied werden. Eine
                    Registrierung steht nur natürlichen und volljährigen Personen offen. Ein Anspruch auf Registrierung
                    als Mitglied besteht nicht. Die Registrierung als Mitglied ist nicht Voraussetzung für die Nutzung
                    der Buchungsfunktion auf Salontime.de.
                    Wenn Sie Sich registrieren wollen, müssen Sie einen Nutzernamen und ein Passwort wählen.
                    Voraussetzung für eine Registrierung ist die außerdem Angabe des Vor- und Nachnamens sowie einer
                    gültigen Email-Adresse. Nach der Anmeldung erhalten Sie von uns eine Email zur Verifizierung Ihrer
                    Email-Adresse. Die Registrierung als Mitglied ist erst abgeschlossen, wenn Sie den in der Email
                    enthaltenen Verifizierungs-Link anklicken und auf Salontime zurückgeleitetet werden.
                    Sie stehen dafür ein, dass Ihre bei der Registrierung uns gegenüber gemachten Angaben wahr und
                    vollständig sind. Veränderungen Ihrer im Rahmen der Registrierung angegebenen Daten müssen Sie uns
                    unverzüglich und unaufgefordert mitteilen.
                    Wir raten Ihnen, Ihre persönlichen Zugangsdaten vertraulich zu behandeln und vor dem Zugriff von
                    unbefugten Dritten zu schützen. Haben Sie Grund zur Annahme, dass Dritte Kenntnis Ihrer Zugangsdaten
                    erlangt haben, bitten wir Sie, uns unverzüglich unter koenigkunde@Salontime.de zu informieren.
                    Als Mitglied haben Sie Zugang zu weiteren Produkten, können Inhalte auf die Webseite hochladen,
                    Kontaktinformationen speichern sowie Informationen zu (Rabatt- oder Promotions-) Aktionen erhalten.
                    Wir behalten uns vor, Ihre Mitgliedschaft jederzeit ohne Angabe von Gründen zu beenden oder
                    diejenigen Nutzungsmöglichkeiten der Webseite, welche die Mitgliedschaft bietet, einzuschränken.
                    Wir behalten uns vor, Ihr Mitgliedskonto zu sperren, wenn wir Grund zur Annahme haben, dass Sie Ihre
                    Verpflichtungen aus diesen AGB verletzt haben.
                    Es ist Ihnen untersagt, sich nach einer Beendigung, Einschränkung oder Sperrung Ihres Mitgliedkontos
                    erneut zu registrieren.<br>


                    Sie können Ihre Mitgliedschaft jederzeit beenden. Dazu benachrichtigen Sie uns bitte mit dem Betreff
                    “ Kündigung des Nutzerkonto” an info@Salontime.de.<br><br>
                    <h5>8. Bewertungsportal</h5>

                    Mitglieder (und unter Umständen auch andere Nutzer der Webseite) können im Rahmen einer
                    Bewertungsfunktion Kommentare über unsere Partner verfassen („Inhalte“) und auf der Webseite
                    veröffentlichen.
                    In diesem Fall wird der Nutzername des Mitglieds verbunden mit den Inhalten veröffentlicht. Ihre
                    Email-Adresse werden wir nicht veröffentlichen.
                    Wir behalten uns vor, solche Inhalte zu löschen, welche fremde Rechte verletzen oder auf sonstige
                    Weise gegen geltendes Recht oder die guten Sitten verstoßen.
                    Ebenso behalten wir uns vor, Ihr Nutzerkonto zu sperren und/oder Sie generell von der
                    Veröffentlichung von Inhalten auszuschließen, wenn Sie fremde Rechte verletzen oder auf sonstige
                    Weise gegen geltendes Recht oder die guten Sitten verstoßen.
                    Wenn Ihnen auf der Webseite Inhalte auffallen, welche Sie in Ihren Rechten verletzen oder auf
                    sonstige Weise gegen geltendes Recht oder die guten Sitten verstoßen, benachrichtigen Sie uns bitte
                    mit dem Betreff “Inhaltsbeschwerde” an info@Salontime.de. Wir werden dann ggf. zwecks Rückfragen mit
                    Ihnen in Kontakt treten, den Inhalt überprüfen und den Inhalt ggfs. entfernen.<br><br>
                    <h5>9. Gutscheinbedingungen</h5>

                    Salontime behält sich vor, im Rahmen von Werbeaktionen Rabattcodes und Gutscheine unentgeltlich
                    auszugeben. Soweit bei Ausgabe nicht ausdrücklich etwas anderes bestimmt wird, gelten folgende
                    Ausgabebedingungen:

                    Die Rabattcodes oder Gutscheine haben eine Gültigkeitsdauer von maximal 2 Monaten ab Ausgabedatum,
                    danach ist eine Einlösung nicht mehr möglich.
                    Die Einlösemenge von Rabattcodes oder Gutscheinen ist immer begrenzt, jeder Rabatt- oder
                    Gutscheincode kann maximal 1000 mal eingelöst werden.
                    Eine Barauszahlung des Gutscheinbetrags ist nicht möglich.<br>
                    Die Rabattcodes oder Gutscheine sind ausschließlich bei teilnehmenden Partnern und ausschließlich
                    bei einer Buchung mit Onlinezahlung Salontime.de einzulösen.
                    Pro Person ist jeweils nur ein Gutschein/Rabattcode einlösbar.<br>
                    Es ist untersagt, den Gutschein- oder Rabattcode auf Gutscheinportalen zu veröffentlichen oder
                    weiterzuverkaufen.<br><br>
                    <h5>10. Verantwortung für Inhalte und Verlinkungen / Haftungsfreistellung</h5>

                    Wir überwachen weder die übermittelten oder gespeicherten Informationen noch suchen wir nach
                    möglicherweise rechtswidrigen Inhalten. Wir verpflichten uns auch nicht, dies zu tun.
                    Sie verpflichten sich, uns von jeglichen Ansprüchen freizustellen, die Dritte gegenüber uns wegen
                    einer durch Sie begangenen Rechts- oder Pflichtverletzung geltend machen, es sei denn, Sie haben die
                    Pflichtverletzung nicht zu vertreten. Dies umfasst insbesondere das rechts- oder vertragswidrige
                    Einstellen von Bewertungen. Zu erstatten sind in diesem Fall auch angemessene Kosten der
                    Rechtsverteidigung (insbes. Anwaltskosten), die uns durch Ihr Fehlverhalten nachweislich entstanden
                    sind.<br><br>
                    <h5>11. Nutzungsrechte</h5>

                    Soweit die von Ihnen auf der Webseite veröffentlichen Inhalte urheberrechtlichen Schutz genießen,
                    räumen Sie uns hiermit die zum Betrieb der Webseite erforderlichen Rechte an diesen Inhalten ein.
                    Dies umfasst insbesondere das Recht, von Ihnen erstellte Inhalte zu speichern und auf Salontime.de
                    öffentlich zugänglich zu machen. Die Einräumung dieser Rechte erfolgt durch Eingabe der Inhalte auf
                    Salontime.de. Salontime nimmt jene Einräumung schon jetzt an.
                    Sie gestatten Salontime darüber hinaus schon jetzt, die von Ihnen erstellten Inhalte (auch unter
                    Entfernung Ihres Nutzernamens) zur Bewerbung von Salontime.de oder einzelner Angebote von Salontime
                    zu verwenden, insbesondere im Rahmen von Internet-, Print- und/oder TV- und Kinowerbung. Salontime
                    nimmt die Einräumung dieser weitergehenden Rechte erst durch Durchführung der jeweiligen konkreten
                    Werbemaßnahme an.
                    Die gemäß Ziffer 7.1 und 7.2 eingeräumten Rechte werden Salontime als einfache, zeitlich und
                    räumlich unbeschränkte Nutzungsrechte eingeräumt. Die gemäß Ziffern 12.1 und 12.2eingeräumten Rechte
                    können von Salontime auf Dritte übertragen und unterlizenziert werden. Salontime nimmt die
                    Rechteeinräumung schon hiermit an. Soweit an den Inhalten kein Urheberrecht entsteht, erklären Sie
                    sich mit der Nutzung der Inhalte im vorgenannten Umfang einverstanden.<br><br>
                    <h5>12. Zugang zur Webseite</h5>

                    Wir bemühen uns um ständige Verfügbarkeit von Salontime.de, können diese aber nicht für jeden
                    Zeitraum gewährleisten. Insbesondere weisen wir hiermit darauf hin, dass wir zu einem großen Teil
                    von der Verfügbarkeit der Dienstleistungen Dritter (z.B. Serviceprovider) abhängig sind, um unsere
                    Dienstleistungen erbringen zu können. Es besteht daher jederzeit die Möglichkeit, dass die Webseite
                    ohne Ankündigung ganz oder teilweise nicht erreichbar ist oder inhaltlich geändert wird (z. B. wegen
                    Updates). Ebenso behalten wir uns vor, auf der Webseite vertretene Partner oder angebotene
                    Dienstleistungen jederzeit ohne Ankündigung von der Webseite zu entfernen.
                    Wir bemühen uns darum, die Informationen auf unserer Webseite aktuell und zutreffend zu halten.
                    Bitte verlassen Sie sich aber nicht vollständig darauf, sondern prüfen Sie die Informationen in
                    allen für Sie wichtigen Fällen selbstständig nach.<br><br>
                    <h5>13. Haftung von Salontime</h5>

                    Salontime ist zu keinem Zeitpunkt zur Erbringung der durch die Partner angebotenen Dienstleistungen
                    verpflichtet und übernimmt folglich auch keine Gewähr für die Mangelfreiheit oder Qualität dieser
                    Dienstleistungen. Die im Profil und auf der Buchungsseite aufgeführten Leistungsbeschreibungen sind
                    solche des jeweiligen Partners und können vereinzelt von dem tatsächlich vom Partner angebotenen
                    Leistungsinhalt abweichen. Salontime macht sich diese Leistungsbeschreibungen nicht zu eigen.
                    Eine Haftung für Fremdverschulden sowie für eine nicht von uns zu vertretende Unterbrechung der
                    Verfügbarkeit von Salontime.de ist ausgeschlossen. Insbesondere bestehen keine Ansprüche gegen
                    Salontime, soweit eine Unterbrechung der Verfügbarkeit Auswirkungen auf das Zustandekommen oder die
                    Durchführung von Verträgen mit den Partnern hat, etwa weil ein Termin nicht gebucht werden kann.
                    Salontime haftet ohne Einschränkung für

                    Ansprüche wegen Schäden aus der Verletzung von Leben, Körper und Gesundheit, Rechte und Ansprüche
                    bei arglistigem Verschweigen eines Mangels durch Salontime oder wegen Fehlens einer Beschaffenheit,
                    für die Salontime eine Garantie übernommen hat, und
                    Ansprüche und Rechte, die auf vorsätzlichem oder grob fahrlässigem Verhalten von Salontime selbst,
                    unserer Arbeitnehmer, gesetzlichen Vertreter oder Erfüllungsgehilfen beruhen, sowie Ansprüche nach
                    dem Produkthaftungsgesetz.

                    Im Übrigen haftet Salontime bei leicht fahrlässiger Schadensverursachung nur bei Verletzung
                    wesentlicher Pflichten (Kardinalpflichten), d.h. von Pflichten, deren Erfüllung zur Erreichung des
                    Vertragszwecks erforderlich ist oder auf deren Einhaltung der Vertragspartner regelmäßig vertrauen
                    darf. Bei Verletzung von Kardinalpflichten ist unsere Haftung begrenzt auf den Ersatz des typischen
                    und bei Vertragsschluss für uns vorhersehbaren Schadens. Im Übrigen ist eine Haftung von Salontime
                    bei leicht oder einfach fahrlässiger Schadensverursachung ausgeschlossen.
                    Soweit die Haftung von Salontime ausgeschlossen oder beschränkt ist, erstreckt sich dieser
                    Ausschluss oder diese Beschränkung auch auf die persönliche Haftung von Arbeitnehmern, gesetzlichen
                    Vertretern oder Erfüllungsgehilfen von Salontime.<br><br>
                    <h5>14. Haftung der Partner</h5>

                    Für das Vertragsverhältnis zwischen Ihnen und dem Partner gelten die gesetzlichen
                    Gewährleistungsrechte.
                    Der jeweilige Partner haftet ohne Einschränkung für

                    Ansprüche wegen Schäden aus der Verletzung von Leben, Körper und Gesundheit, Rechte und Ansprüche
                    bei arglistigem Verschweigen eines Mangels durch den Partner oder wegen Fehlens einer
                    Beschaffenheit, für die der Partner eine Garantie übernommen hat, und,
                    Ansprüche und Rechte, die auf vorsätzlichem oder grob fahrlässigem Verhalten des Partners selbst,
                    seiner Arbeitnehmer, gesetzlichen Vertreter oder Erfüllungsgehilfen beruhen sowie Ansprüche nach dem
                    Produkthaftungsgesetz.<br>

                    Im Übrigen haftet der Partner bei leicht fahrlässiger Schadensverursachung nur bei Verletzung
                    wesentlicher Pflichten (Kardinalpflichten), d.h. von Pflichten, deren Erfüllung zur Erreichung des
                    Vertragszwecks erforderlich ist oder auf deren Einhaltung Sie regelmäßig vertrauen dürfen. Bei
                    Verletzung von Kardinalpflichten ist die Haftung begrenzt auf den Ersatz des typischen und bei
                    Vertragsschluss für den Partner vorhersehbaren Schadens. Im Übrigen ist eine Haftung des Partners
                    bei leicht oder einfach fahrlässiger Schadensverursachung ausgeschlossen.
                    Soweit die Haftung des Partners ausgeschlossen oder beschränkt ist, erstreckt sich dieser Ausschluss
                    oder diese Beschränkung auch auf die persönliche Haftung von Arbeitnehmern, gesetzlichen Vertretern
                    oder Erfüllungsgehilfen des Partners.<br><br>
                    <h5>15. Datenschutz</h5>

                    Salontime hält sich an die gesetzlichen Datenschutzbestimmungen. Unsere Datenschutzerklärung finden
                    Sie <a href="/datenschutz">hier</a>. Darin erklären wir, wie wir mit Ihren personenbezogenen Daten
                    umgehen und wie wir Ihre Daten schützen, wenn Sie unsere Dienste nutzen. Durch die Nutzung von
                    Salontime.de stimmen Sie einer Verwendung dieser Daten wie in der Datenschutzerklärung dargelegt zu.<br><br>
                    <h5>16. Kündigung</h5>

                    Der Vertrag über die Nutzung von Salontime.de zwischen Salontime und Ihnen kann jederzeit von uns
                    oder von Ihnen mit sofortiger Wirkung gekündigt werden. Die Kündigung bedarf keiner Angabe von
                    Gründen und hat in Textform (z.B. E-Mail) zu erfolgen. Ein etwaiges Vertragsverhältnis zwischen
                    Ihnen und einem Partner bleibt davon unberührt.<br><br>
                    <h5>17. Schlussbestimmungen</h5>

                    Sollten einzelne der vorstehenden Bestimmungen unwirksam sein, hat dies nicht die Unwirksamkeit der
                    übrigen Bestimmungen zur Folge.
                    Es gilt ausschließlich das Recht der Bundesrepublik Deutschland. Eine Anwendung des UN-Kaufrechts
                    ist ausgeschlossen.
                    Wenn Sie Fragen/Anregungen haben oder Bedenken bezüglich der Materialien auf der Webseite haben,
                    schreiben Sie uns bitte an info@Salontime.de.
                    Sind Sie Kaufmann oder vertreten Sie eine juristische Personen des öffentlichen Rechts oder
                    öffentlich-rechtliches Sondervermögen, vereinbaren wir hiermit Berlin als ausschließlichen
                    Gerichtsstand.

                </div>
                <div class="col-md-2"></div>
            </div>
        </section>
        <!-- END PRICING SECTION -->
    </section>

@endsection

@section('scripts')
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="/assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script src="assets_frontend/js/custom.js" type="text/javascript"></script>
@endsection