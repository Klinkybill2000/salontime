@extends ('layouts.pages')

@section ('content')
    <section class="m-t-100 sm-m-t-30">
        <!-- START PORTFOLIO SECTION -->
        <section class="p-b-40 p-t-55">
            <div class="container">
                <div class="m-b-30">
                    <h1 class="text-center ">Salontime ist vollgepackt mit <b>Features,</b> <br>die Deinen Alltag
                        erleichtern.</h1>
                    <div class="b-b b-grey m-t-30"></div>
                    <div class="col-md-9 col-md-offset-3 col-sm-8 col-sm-offset-2 p-t-15 p-b-15">
                        <ul class="no-style no-margin text-center">
                            <li class="m-r-20 m-l-20 inline sm-block text-center sm-p-b-5 sm-m-b-5 b-grey sm-b-b">
                                <a href="#" class="link active text-black fs-13 block-title ">Salonverwaltung</a>
                            </li>
                            <li class="m-r-20 m-l-20 inline sm-block text-center sm-p-b-5 sm-m-b-5 b-grey sm-b-b">
                                <a href="#" class="link active text-black fs-13 block-title">Online Termine</a>
                            </li>
                            <li class="m-r-20 m-l-20 inline sm-block text-center sm-p-b-5 sm-m-b-5 b-grey sm-b-b">
                                <a href="#" class="link active text-black fs-13 block-title">Kundenverwaltung</a>
                            </li>
                            <li class="m-r-20 m-l-20 inline sm-block text-center sm-p-b-5 sm-m-b-5 b-grey sm-b-b">
                                <a href="#" class="link active text-black fs-13 block-title">Benachrichtigungen</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="gallery">
                <div class="grid-sizer col-sm-3 col-xs-6 no-padding"></div>
                <!-- START GALLERY ITEM -->
                <div class="gallery-item col-sm-6 col-xs-12 no-padding hover-zoom">
                    <div class="inner bottom-left bottom-right padding-30">
                        <h5 class="block-title text-white no-margin">Salonverwaltung</h5>
                        <h6 class="block-title fs-11 hint-text text-white m-t-5 m-b-0"></h6>
                        <p class="font-arial fs-11 text-white muted m-t-5 m-b-0">Nutze Deine Kapazitäten effizienter</p>
                    </div>
                    <div class="ar-2-1">
                        <div data-pages-bg-image="assets/images/gallery_1.jpg" data-bg-overlay="black"
                             data-overlay-opacity="0.4">
                        </div>
                    </div>
                </div>
                <!-- END GALLERY ITEM -->
                <!-- START GALLERY ITEM -->
                <div class="gallery-item col-sm-3 col-xs-6 no-padding hover-zoom">
                    <div class="inner bottom-left bottom-right padding-30">
                        <h5 class="block-title text-white no-margin">Kundenverwaltung</h5>
                        <h6 class="block-title fs-11 hint-text text-white m-t-5 m-b-0"></h6>
                        <p class="font-arial fs-11 text-white muted m-t-5 m-b-0">Erstelle genaue Kundenstatistiken</p>
                    </div>
                    <div class="ar-1-1">
                        <!-- START PREVIEW -->
                        <div data-pages-bg-image="assets/images/gallery_2.jpg" data-bg-overlay="black"
                             data-overlay-opacity="0.4">
                        </div>
                        <!-- END PREVIEW -->
                    </div>
                </div>
                <!-- END GALLERY ITEM -->
                <div class="gallery-item col-sm-3 col-xs-6 no-padding hover-zoom">
                    <div class="inner bottom-left bottom-right padding-30">
                        <h5 class="block-title text-white no-margin">Benachrichtigungen</h5>
                        <h6 class="block-title fs-11 hint-text text-white m-t-5 m-b-0"></h6>
                        <p class="font-arial fs-11 text-white muted m-t-5 m-b-0">Email System, damit du keinen Termin
                            vergisst</p>
                    </div>
                    <div class="ar-1-1">
                        <!-- START PREVIEW -->
                        <div data-pages-bg-image="assets/images/gallery_6.jpg" data-bg-overlay="black"
                             data-overlay-opacity="0.4">
                        </div>
                        <!-- END PREVIEW -->
                    </div>
                </div>
                <div class="gallery-item col-sm-3 col-xs-6 no-padding hover-zoom">
                    <div class="inner bottom-left bottom-right padding-30">
                        <h5 class="block-title text-white no-margin">Buchungsseite</h5>
                        <h6 class="block-title fs-11 hint-text text-white m-t-5 m-b-0"></h6>
                        <p class="font-arial fs-11 text-white muted m-t-5 m-b-0">Stelle Deinen Salon vor</p>
                    </div>
                    <div class="ar-1-1">
                        <!-- START PREVIEW -->
                        <div data-pages-bg-image="assets/images/gallery_4.jpg" data-bg-overlay="black"
                             data-overlay-opacity="0.4">
                        </div>
                        <!-- END PREVIEW -->
                    </div>
                </div>
                <div class="gallery-item col-sm-3 col-xs-6 no-padding hover-zoom">
                    <div class="inner bottom-left bottom-right padding-30">
                        <h5 class="block-title text-white no-margin">Entspannte Verwaltung</h5>
                        <h6 class="block-title fs-11 hint-text text-white m-t-5 m-b-0"></h6>
                        <p class="font-arial fs-11 text-white muted m-t-5 m-b-0">Statistiken und Auswertungen auf einen
                            Blick</p>
                    </div>
                    <div class="ar-1-1">
                        <!-- START PREVIEW -->
                        <div data-pages-bg-image="assets/images/gallery_3.jpg" data-bg-overlay="black"
                             data-overlay-opacity="0.4">
                        </div>
                        <!-- END PREVIEW -->
                    </div>
                </div>
                <div class="gallery-item col-sm-6 col-xs-12 no-padding hover-zoom">
                    <div class="inner bottom-left bottom-right padding-30">
                        <h5 class="block-title text-white no-margin">Überall Termine vereinbaren</h5>
                        <h6 class="block-title fs-11 hint-text text-white m-t-5 m-b-0"></h6>
                        <p class="font-arial fs-11 text-white muted m-t-5 m-b-0">24 / 7</p>
                    </div>
                    <div class="ar-2-1">
                        <div data-pages-bg-image="assets/images/gallery_5.jpg" data-bg-overlay="black"
                             data-overlay-opacity="0.4">
                        </div>
                    </div>
                </div>
                <!-- END GALLERY ITEM -->
            </div>
        </section>
        <!-- END PORTFOLIO SECTION -->
        <!-- BEGIN CONTENT SECTION -->
        <section class="p-b-70 p-t-40">
            <div class="container">
                <h5 class="block-title hint-text no-margin">Über Salontime</h5>
                <h5>
                    Unsere langjährige Vision war es, die übliche Dashboard-Struktur voranzutreiben mit einem
                    Anspruchsvollen, aber einfachen System. So schaffen wir die Rahmenbedingungen für einen erfolgreich
                    geführten Salon.</h5>
            </div>
        </section>
        <!-- END CONTENT SECTION -->
        <section class="p-b-70 p-t-70 demo-hero-5" data-pages-bg-image="assets/images/gallery_hero_2.jpg">
        </section>
        <!-- BEGIN CONTENT SECTION -->
        <section class="bg-master-lightest p-b-85 p-t-75">
            <div class="container">
                <div class="md-p-l-20 md-p-r-20 xs-no-padding">
                    <h5 class="block-title hint-text no-margin">Entdecke Salontime</h5>
                    <div class="row">
                        <div class="col-sm-5 col-md-4">
                            <h1 class="m-t-5">Unsere <br>Partner vertrauen uns.</h1>
                        </div>
                        <div class="col-sm-7 col-md-8 no-padding xs-p-l-15 xs-p-r-15">
                            <img src="assets/images/logo_set.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

@section('scripts')
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <!-- Waits for the images to be loaded before applying the Isotope plugin -->
    <script src="assets_frontend/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <!-- Isotope plugin arranges the card layout -->
    <script src="assets_frontend/plugins/jquery-isotope/isotope.pkgd.min.js" type="text/javascript"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script src="assets_frontend/js/custom.js" type="text/javascript"></script>
    <script src="assets_frontend/js/gallery.js" type="text/javascript"></script>
@endsection