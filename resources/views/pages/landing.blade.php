@extends('layouts.pages')

@section('content')
    <div class="page-wrappers">
        <!-- BEGIN JUMBOTRON -->
        <section class="jumbotron full-vh" data-pages="parallax">
            <div class="inner full-height">
                <!-- BEGIN SLIDER -->
                <div class="swiper-container" id="hero">
                    <div class="swiper-wrapper">
                        <!-- BEGIN SLIDE -->
                        <div class="swiper-slide fit">
                            <!-- BEGIN IMAGE PARRALAX -->
                            <div class="slider-wrapper">
                                <div class="background-wrapper" data-swiper-parallax="30%">
                                    <!-- YOUR BACKGROUND IMAGE HERE, YOU CAN ALSO USE IMG with the same classes -->
                                    <div class="background" data-pages-bg-image="assets/images/hero_1.jpg"></div>
                                </div>
                            </div>
                            <!-- END IMAGE PARRALAX -->
                            <!-- BEGIN CONTENT -->
                            <div class="content-layer">
                                <div class="inner full-height">
                                    <div class="container-xs-height full-height">
                                        <div class="col-xs-height col-middle text-left">
                                            <div class="container">
                                                <div class="col-md-6 no-padding col-xs-10 col-xs-offset-1">
                                                    <h1 class="light sm-text-center" data-swiper-parallax="-15%"> Deine
                                                        neue Salonverwaltung mit endlosen Möglichkeiten.</h1>
                                                    <h4 class="sm-text-center">Spare Zeit und Geld und gewinne mehr
                                                        Kunden durch einen professionellen Auftritt.</h4>
                                                    <h5 class="block-title text-white sm-text-center">
                            <span class="p-t-10 input-group-btn">
                            <a href="/register"><button class="btn btn-danger  btn-cons" type="button"><i
                                            class="fa fa-sign-in">&nbsp;</i>Account erstellen</button></a>
                            </span>
                                                    </h5>
                                                    <p class="p-t-10 fs-12 font-arial hint-text sm-text-center">Verwalte
                                                        Termine, Kunden, Mitarbeiter Zeitpläne und vieles mehr...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END CONTENT -->
                        </div>
                        <!-- END SLIDE -->
                        <!-- BEGIN SLIDE -->
                        <div class="swiper-slide fit">
                            <!-- BEGIN IMAGE PARRALAX -->
                            <div class="slider-wrapper">
                                <div class="background-wrapper" data-swiper-parallax="30%">
                                    <!-- YOUR BACKGROUND IMAGE HERE, YOU CAN ALSO USE IMG with the same classes -->
                                    <div data-pages-bg-image="assets/images/hero_2_new.jpg" draggable="false"
                                         class="background"></div>

                                </div>
                            </div>
                            <!-- END IMAGE PARRALAX -->
                            <!-- BEGIN CONTENT -->
                            <div class="content-layer">
                                <div class="inner full-height">
                                    <div class="container-xs-height full-height">
                                        <div class="col-xs-height col-middle text-left">
                                            <div class="container">
                                                <div class="top-left col-md-6 m-t-100 col-xs-10 col-xs-offset-1">
                                                    <h1 class="light m-t-60" data-swiper-parallax="-15%"> Online
                                                        Terminbuchung<br> inklusive...</h1>
                                                    <span class="p-t-20 input-group-btn">
                            <a href="/register"><button class="btn btn-danger  btn-cons" type="button"><i
                                            class="fa fa-sign-in">&nbsp;</i>Account erstellen</button></a>
                            </span>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END CONTENT -->
                        </div>
                        <!-- END SLIDE -->
                        <!-- BEGIN SLIDE -->
                        <div class="swiper-slide fit">
                            <!-- BEGIN IMAGE PARRALAX -->
                            <div class="slider-wrapper">
                                <div class="background-wrapper" data-swiper-parallax="30%">
                                    <div data-pages-bg-image="assets/images/hero_5.jpeg" draggable="false"
                                         class="background"></div>
                                </div>
                            </div>
                            <!-- END IMAGE PARRALAX -->
                            <!-- BEGIN CONTENT -->
                            <div class="content-layer">
                                <div class="inner full-height">
                                    <div class="container-xs-height full-height">
                                        <div class="col-xs-height col-middle text-left">
                                            <div class="container">
                                                <div class="col-sm-6 col-sm-offset-3 text-center">
                                                    <h1 class="light m-t-80" data-swiper-parallax="-15%">Komfortabel
                                                        gewinnt!
                                                    </h1>
                                                    <span class="p-t-20 input-group-btn">
                            <a href="/register"><button class="btn btn-danger btn-cons" type="button"><i
                                            class="fa fa-sign-in">&nbsp;</i>Account erstellen</button></a>
                            </span>
                                                    <p class="p-t-20 fs-12 font-arial hint-text">Melde Dich jetzt bei
                                                        Salontime an und sichere Dir ein attraktives Paket.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END CONTENT -->
                        </div>
                        <!-- END SLIDE -->
                    </div>
                    <!-- BEGIN ANIMATED MOUSE -->
                    <div class="mouse-wrapper">
                        <div class="mouse">
                            <div class="mouse-scroll"></div>
                        </div>
                    </div>
                    <!-- Add Navigation -->
                    <div class="swiper-navigation swiper-dark-solid swiper-button-prev auto-reveal"></div>
                    <div class="swiper-navigation swiper-dark-solid swiper-button-next auto-reveal"></div>
                </div>
            </div>
            <!-- END SLIDER -->
        </section>
        <!-- END JUMBOTRON -->
        <!-- BEGIN CONTENT SECTION -->
        <section class="bg-master-lightest p-b-85 p-t-75">
            <div class="container">
                <div class="md-p-l-20 md-p-r-20 xs-no-padding">
                    <h5 class="block-title hint-text no-margin">Entdecke Salontime</h5>
                    <div class="row">
                        <div class="col-sm-5 col-md-4">
                            <h1 class="m-t-5">Ein neuer Weg, Deinen Salon zu organisieren.</h1>
                        </div>
                        <div class="col-sm-7 col-md-8 no-padding xs-p-l-15 xs-p-r-15">
                            <div class="p-t-20 p-l-35 md-p-l-5 md-p-t-15">
                                <p>Unsere Vision ist es, Dein alltägliches Arbeitsleben zu vereinfachen. Durchbrich
                                    Deine alten Strukturen und gehe mit uns einen neuen Weg. Unsere Anspruchsvolle aber
                                    für den Benutzer einfach handzuhabende App erleichtert Dir Deine Arbeit. Jeden Tag
                                    auf's neue. Manage Deinen Salon mit Salontime.</p>
                                <p class="hint-text font-arial small-text col-md-7 no-padding">Ein Produkt ist dann
                                    zugänglich, wenn alle Menschen, unabhängig von Ihren Fähigkeit, ihre Ziele
                                    erreichen.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END CONTENT SECTION -->
        <section class="text-center">
            <img src="assets/images/showcase.png" alt="" class="image-responsive-width sm-image-responsive-height lazy">
        </section>
        <!-- BEGIN CONTENT SECTION -->
        <section class=" p-t-75  p-t-55">
            <div class="container">
                <div class="md-p-l-20 xs-no-padding clearfix">
                    <div class="col-sm-4 no-padding">
                        <div class="p-r-40 md-pr-30">
                            <img alt="" class="m-b-20" src="assets/images/Parachute.svg"
                                 style="height: 60px; width: 60px;">
                            <h6 class="block-title p-b-5">Online Terminverwaltung <i class="pg-arrow_right m-l-10"></i>
                            </h6>
                            <p class="m-b-30">Buche, verwalte und verfolge Deine Termine online mit Salontime. Unsere
                                Cloud-basierte App bietet Dir die volle Kontrolle über Deine Termine. Immer.</p>
                            <p class="muted font-arial small-text col-sm-9 no-padding">Biete Deinen Kunden die
                                Möglichkeit der Online Buchung und profitiere von weniger abgesagten Terminen.</p>
                        </div>
                        <div class="visible-xs b-b b-grey-light m-t-30 m-b-30"></div>
                    </div>
                    <div class="col-sm-4 no-padding">
                        <div class="p-r-40 md-pr-30">
                            <img alt="" class="m-b-20" src="assets/images/Prizemedalion.svg"
                                 style="height: 60px; width: 60px;">
                            <h6 class="block-title p-b-5">Kundenverwaltung <i class="pg-arrow_right m-l-10"></i></h6>
                            <p class="m-b-30">Deine Kunden sind Dein Kapital. Verwalte Deine Kunden mit Salontime
                                effizient und erreiche Sie durch unsere Plattform wesentlich besser und einfacher.</p>
                            <p class="muted font-arial small-text col-sm-9 no-padding">Salontime bietet Dir unendliche
                                Möglichkeiten und ist einzigartig intuitiv.</p>
                        </div>
                        <div class="visible-xs b-b b-grey-light m-t-30 m-b-30"></div>
                    </div>
                    <div class="col-sm-4 no-padding">
                        <div class="p-r-40 md-pr-30">
                            <img alt="" class="m-b-20" src="assets/images/Umbrella.svg"
                                 style="height: 60px; width: 60px;">
                            <h6 class="block-title p-b-5">Mitarbeiterverwaltung <i class="pg-arrow_right m-l-10"></i>
                            </h6>
                            <p class="m-b-30">Bleibe organisiert und steigere Deine Produktivität durch einfache
                                Verwaltung Deiner Mitarbeiter-Zeitpläne. So kannst du besser planen.</p>
                            <p class="muted font-arial small-text col-sm-9 no-padding">Durch die Effiziente
                                Mitarbeiterverwaltung hast Du die Auslastung Deines Salons und die Kapazitäten stets im
                                Blick.</p>
                        </div>
                        <div class="visible-xs b-b b-grey-light m-t-50 m-b-50"></div>
                    </div>

                    <div class="col-sm-4 m-t-50 m-b-50">
                        <div class="p-r-40 md-pr-30">
                            <img alt="" class="m-b-20" src="assets/images/alarm.svg" style="height: 60px; width: 60px;">
                            <h6 class="block-title p-b-5 ">Autmatische Erinnerung <i class="pg-arrow_right m-l-10"></i>
                            </h6>
                            <p class="m-b-30">Bekomme sofortige E-Mail Erinnerungen für jeden Termin, der gebucht wird,
                                abgesagt oder umgeplant wird. Sag Adieu zu verpassten Terminen.</p>

                        </div>
                        <div class="visible-xs b-b b-grey-light m-t-30 m-b-30"></div>
                    </div>
                    <div class="col-sm-4 m-t-50 m-b-50">
                        <div class="p-r-40 md-pr-30">
                            <img alt="" class="m-b-20" src="assets/images/phone.svg" style="height: 60px; width: 60px;">
                            <h6 class="block-title p-b-5">Mobile Ready <i class="pg-arrow_right m-l-10"></i></h6>
                            <p class="m-b-30">Salontime wurde speziell für Mobile Endgeräte entwickelt. So hast Du
                                überall und Jederzeit Deinen Salon im Blick. So hast Du Salontime immer dabei.</p>

                        </div>
                        <div class="visible-xs b-b b-grey-light m-t-30 m-b-30"></div>
                    </div>
                    <div class="col-sm-4 m-t-50 m-b-50">
                        <div class="p-r-40 md-pr-30">
                            <img alt="" class="m-b-20" src="assets/images/robot.svg" style="height: 60px; width: 60px;">
                            <h6 class="block-title p-b-5">Buchungen wie von selbst <i class="pg-arrow_right m-l-10"></i>
                            </h6>
                            <p class="m-b-30">Deine Kunden können die verfügbaren Zeitfenster ansehen und Termine mit
                                nur wenigen klicks buchen. Es ist einfach, intuitiv und geht schnell.</p>

                        </div>
                        <div class="visible-xs b-b b-grey-light m-t-30"></div>
                    </div>
                </div>

            </div>
        </section>
        <!-- END CONTENT SECTION -->
        <!-- START CONTENT SECTION -->
        <section id="demo-content-3" class="jumbotron relative no-overflow bg-white">
            <img class="demo-browser-desktop visible-lg visible-xlg" src="assets/images/browser_desktop.jpg"
                 data-pages="float" data-max-top-translate="0" data-max-bottom-translate="40" data-speed="0.1"
                 data-delay="1000" data-curve="ease" alt="">
            <img class="demo-browser-ipad visible-sm visible-md" src="assets/images/browser_ipad.jpg" data-pages="float"
                 data-max-top-translate="0" data-max-bottom-translate="40" data-speed="0.1" data-delay="1000"
                 data-curve="ease" alt="">
            <img class="demo-browser-phone visible-xs" src="assets/images/browser_phone.jpg" data-pages="float"
                 data-max-top-translate="0" data-max-bottom-translate="40" data-speed="0.1" data-delay="1000"
                 data-curve="ease" alt="">
            <div class="demo-shadow"></div>
        </section>
        <!-- END CONTENT SECTION -->
        <!-- START CONTENT SECTION -->
        <section class="container container-fixed-lg p-t-60 p-b-60">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="pull-left sm-pull-reset text-center m-t-5">
                            </div>
                            <p class="fs-16 no-margin pull-left p-l-20 sm-pull-reset sm-text-center sm-no-padding  sm-m-t-20">
                                Salontime bietet Dir viele durchdachte
                                <br> Funktionen für Deinen Salon.</p>
                        </div>
                        <div class="col-md-5 text-right sm-text-center">
                            <a href="/register">
                                <button class="btn btn-danger  btn-cons">Ja, Ich möchte mich anmelden</button>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END CONTENT SECTION -->


        <!-- BEGIN CONTENT SECTION -->
        <section class="demo-bg-section p-b-85 p-t-75 no-overflow">
            <div class="container">
                <div class="md-p-l-20 md-p-r-20 xs-no-padding">
                    <h5 class="block-title m-b-0 text-menu hint-text">Responsive Design</h5>
                    <div class="row">
                        <div class="col-sm-5">
                            <h1 class="m-t-5 m-b-20 text-white">Manage einfach Deinen Salon. Von überall. Immer.</h1>
                            <p class="m-b-20 text-menu">Unserer Responsiver Aufbau stellt die optimale Darstellung auf
                                allen Endgeräten sicher. Damit Du auch von Deinem Handy oder Tablet immer den Überblick
                                über Deine Termine und Deinen Salon behältst.</p>
                            <p class="hint-text font-arial small-text text-menu col-md-8 no-padding">Hast Du dir schon
                                mal Deine Tagesumsätze schon einmal bequem Abends auf der Couch angesehen? Wenn nicht,
                                wird es höchste Zeit, sich bei Salontime anzumelden.</p>
                        </div>
                        <div class="col-sm-7 no-padding xs-p-l-15 xs-p-r-15">
                            <div class="content-mask-md">
                                <div class="device_morph pull-center-inner" id="iphone01" data-pages="float"
                                     data-max-top-translate="40" data-max-bottom-translate="300" data-speed="-0.1"
                                     data-delay="1000" data-curve="ease">
                                    <img alt="" class="xs-image-responsive-height image-responsive-width"
                                         src="assets/images/b_phone.png">
                                    <div class="screen">
                                        <div class="iphone-border">
                                            <img src="assets/images/mobile_preview.jpg"
                                                 class="image-responsive-height lazy" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END CONTENT SECTION -->


        <section class="p-b-85 p-t-75 ">
            <!-- BEGIN TESTIMONIALS SLIDER-->
            <div class="container" id="container">
                <div class="container">
                    <!-- BEGIN TESTIMONIAL -->
                    <div class="container">
                        <div class="container">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h3 class="text-center">
                                    Unser Ziel ist es Kunden und Salons so einfach und <br>schnell wie möglich zusammen
                                    zu bringen.
                                    Aus diesem Ziel geht unser Motto hervor: "Perfektion vereinfacht alles".
                                    <img alt="" class="m-t-20 center" src="assets/images/signature_sample.png">
                                </h3>

                            </div>
                        </div>
                    </div>
                    <!-- END TESTIMONIAL -->

                </div>
                <!-- Add Navigation -->

            </div>
            <!-- END TESTIMONIALS -->
        </section>


        <section class="p-t-55 bg-success-light scroll-x-hidden" data-pages-bg-image="assets/images/work.jpg"
                 data-pages="parallax">
            <div class="container p-b-100">
                <h5 class="block-title text-white ">Kundenstimmen</h5>
                <div class="row p-b-65 p-t-55">
                    <div class="col-sm-6">
                        <h1 class="m-t-5 light text-white">
                            Schon über 2700 Salons
                            vertrauen uns. Werde auch Du Teil
                            der Geschichte.
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="progress progress-small transparent-white m-t-15">
                                    <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="80"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    </div>
                                </div>
                                <h3 class="text-white no-margin lh-large">215%</h3>
                                <p class="text-white fs-12 font-arial">Einfacher
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <div class="progress progress-small transparent-white m-t-15">
                                    <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
                                    </div>
                                </div>
                                <h3 class="text-white no-margin lh-large">174%</h3>
                                <p class="text-white fs-12 font-arial">Schneller
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <div class="progress progress-small transparent-white m-t-15">
                                    <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="55"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                                    </div>
                                </div>
                                <h3 class="text-white no-margin lh-large">165%</h3>
                                <p class="text-white fs-12 font-arial">Erreichbarer
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <div class="progress progress-small transparent-white m-t-15">
                                    <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="95"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                                    </div>
                                </div>
                                <h3 class="text-white no-margin lh-large">300%</h3>
                                <p class="text-white fs-12 font-arial">Cooler
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-white font-arial small-text m-b-50">VIA Salontime www.salontime.de/latest</p>
                <div class="content-triangle"></div>
            </div>
        </section>
        <section class="p-b-55 p-t-25">
            <!-- BEGIN TESTIMONIALS SLIDER-->
            <div class="swiper-container" id="testimonials_slider">
                <div class="swiper-wrapper">
                    <!-- BEGIN TESTIMONIAL -->
                    <div class="swiper-slide">
                        <div class="container text-left">
                            <h5 class="semi-bold m-b-30">- Marc Schmitz</h5>
                            <h3 class="m-b-30">“Perfekt! Ich liebe Salontime einfach. Ich manage einen Haarsalon und
                                Salontime ist bei weitem das beste und zuverlässigste Programm für die Verwaltung von
                                Terminen.”</h3>
                            <h6 class="block-title hint-text fs-12">SALONMANGER KOPFKUNST, KÖLN</h6>
                        </div>
                    </div>
                    <!-- END TESTIMONIAL -->
                    <!-- BEGIN TESTIMONIAL -->
                    <div class="swiper-slide">
                        <div class="container text-left">
                            <h5 class="semi-bold m-b-30">- Laura Cesare</h5>
                            <h3 class="m-b-30">“Salontime entlastet mich total. Ich kann mich dadurch auf das
                                Wesentliche konzentrieren und telefoniere nur noch die Hälfte”</h3>
                            <h6 class="block-title hint-text fs-12">MAKEUP-ARTIST, BEAUTY CONCEPT, BERLIN</h6>
                        </div>
                    </div>
                    <!-- END TESTIMONIAL -->
                    <!-- BEGIN TESTIMONIAL -->
                    <div class="swiper-slide">
                        <div class="container text-left">
                            <h5 class="semi-bold m-b-30">- Jana Nester</h5>
                            <h3 class="m-b-30">“Warum kenne ich Salontime erst so kurz. Ich hätte so viel mehr Freizeit
                                haben können”</h3>
                            <h6 class="block-title hint-text fs-12">BESITZER CHIC HAIR, FRANKFURT</h6>
                        </div>
                    </div>
                    <!-- END TESTIMONIAL -->
                </div>
                <!-- Add Navigation -->
                <div class="container">
                    <div class="swiper-pagination relative pull-left"></div>
                </div>
            </div>
            <!-- END TESTIMONIALS -->
        </section>
    </div>
    <!-- START OVERLAY SEARCH -->
    <div class="overlay hide" data-pages="search">
        <!-- BEGIN Overlay Content !-->
        <div class="overlay-content full-height has-results">
            <!-- END Overlay Header !-->
            <div class="container relative full-height">
                <!-- BEGIN Overlay Header !-->
                <div class="container-fluid">
                    <!-- BEGIN Overlay Close !-->
                    <a href="#" class="close-icon-light overlay-close text-black fs-16 top-right">
                        <i class="pg-close_line"></i>
                    </a>
                    <!-- END Overlay Close !-->
                </div>
                <div class="container-fluid">
                    <div class="inline-block bottom-right m-b-30">
                        <div class="checkbox right">
                            <input id="checkboxn" type="checkbox" value="1" checked="checked">
                            <label for="checkboxn">Search within page</label>
                        </div>
                    </div>
                </div>
                <div class="container-xs-height full-height">
                    <div class="col-xs-height col-middle text-center">
                        <!-- BEGIN Overlay Controls !-->
                        <input id="overlay-search"
                               class="no-border overlay-search bg-transparent col-sm-6 col-sm-offset-4"
                               placeholder="Search..." autocomplete="off" spellcheck="false">
                        <br>
                        <div class="inline-block bottom-left m-l-10 m-b-30 hidden-xs">
                            <p class="fs-14"><i class="fa fa-search m-r-10"></i> Press enter to search</p>
                        </div>
                        <!-- END Overlay Controls !-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END Overlay Content !-->
    </div>
@endsection

@section('scripts')

    <script type="text/javascript" src="assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <script type="text/javascript" src="assets_frontend/plugins/velocity/velocity.min.js"></script>
    <script type="text/javascript" src="assets_frontend/plugins/velocity/velocity.ui.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->

    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->

    <!-- BEGIN YOUR CUSTOM JS -->
    <script type="text/javascript" src="assets_frontend/js/custom.js"></script>
    <!-- END PAGES LIB -->

@endsection