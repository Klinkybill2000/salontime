@extends ('layouts.pages')

@section ('content')
    <section class="jumbotron demo-custom-height-1 full-width" data-pages-bg-image="assets/images/Hero.jpg"
             data-pages="parallax" data-bg-overlay="black" data-overlay-opacity="0.4">
        <div class="inner full-height">
            <div class="container-xs-height full-height">
                <div class="col-xs-height col-middle text-left">
                    <div class="container">
                        <div class="col-sm-6 col-sm-offset-3 sm-m-t-30 sm-p-t-20 sm-p-b-20">
                            <h1 class="light text-center text-white xs-p-t-30 sm-p-b-30">
                                Blog
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END JUMBOTRON -->
    <section class="p-b-50 p-t-50">
        <div class="container">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                    <li class="active">
                        <a data-toggle="tab" href="#tab3hellowWorld">One</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab3FollowUs">Two</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab3Inspire">Three</a>
                    </li>
                </ul>
                <div class="tab-content bg-white ">
                    <div class="row p-l-20 p-r-20 p-b-20 p-t-5 xs-no-padding">
                        <div class="col-md-11">
                            <div class="tab-pane active" id="tab3hellowWorld">
                                <h5 class="font-montserrat text-uppercase fs-14 hint-text">Explore</h5>
                                <h1>“Crafted specially, giving attention to detail, this is a
                                    celebration of creativity with guaranteed smoothness in UI/UX”</h1>
                                <p class="hint-text fs-12">A product is accessible when all people—regardless of
                                    ability—can
                                    <br> navigate it, understand it, and use it to achieve their goals.</p>
                                <br>
                                <p class="hint-text small-text">Our long standing vision has been to bypass the usual
                                    admin dashboard structure, and move forward with a more sophisticated yet simple
                                    framework that would create a credible impact on the many conventional dashboard
                                    users.</p>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6 class="block-title">Launch Your
                                            Design</h6>
                                        <p class="hint-text">Each component is coded for web which will sufficiently
                                            hasten the process of creating a website.</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="block-title">Shop
                                            ready</h6>
                                        <p class="hint-text">Each component is coded for web which will sufficiently
                                            hasten the process of creating a website.</p>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <p class="fs-15 font-open-sans">Funny how you can always tell when somebody's laughing
                                    behind your back. Jodie hadn't really heard anything, maybe a whisper, but when she
                                    turned around, the girls in the back row of the class were looking at her, trying to
                                    hide smiles and giggles. She looked back at her teacher.
                                    <br>
                                    <br> Mr Swales was talking about what people do all day. He also wanted to find out
                                    what his students wanted to be when they grew up. He called on Billy Mitzer first.
                                    <br> Jodie liked it when Mr Swales asked them questions like this. He was about to
                                    call on Jodie when the girls in the back row burst out laughing. Everybody in the
                                    class laughed out loud. Everybody except Jodie, that is. She felt her face turn
                                    bright red. She looked around the whole classroom. Everyone was laughing. Some kids
                                    were even holding their noses.
                                    <br>
                                    <br> The laughter stopped immediately. The sound of cars and people going by out on
                                    the street came through the windows. "You should be ashamed of yourselves," Mr
                                    Swales said. "Being a garbageman...I mean, er, uhm...a Sanitation Engineer, is a
                                    difficult and enormously useful job. We should all be grateful to Mr. Harris. Where
                                    would we be without him? Up to our ears in garbage, that's where. How would you like
                                    that?"</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3FollowUs">
                        <h3>“ Nothing is <span class="semi-bold">impossible</span>, the word itself
                            says 'I'm <span class="semi-bold">possible</span>'! ”</h3>
                        <p>A style represents visual customizations on top of a layout. By editing a style, you can use
                            Squarespace's visual interface to customize your...</p>
                        <br>
                        <p class="pull-right">
                            <button class="btn btn-default btn-cons" type="button">White</button>
                            <button class="btn btn-success btn-cons" type="button">Success</button>
                        </p>
                    </div>
                    <div class="tab-pane" id="tab3Inspire">
                        <h3>Follow us &amp; get updated!</h3>
                        <p>Instantly connect to what's most important to you. Follow your friends, experts, favorite
                            celebrities, and breaking news.</p>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section ('scripts')
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="/assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script src="assets_frontend/js/custom.js" type="text/javascript"></script>
@endsection