@extends ('layouts.pages')

@section('content')
    <section class="p-t-100 sm-p-t-30 p-b-30 bg-master-lighter">
        <!-- BEGIN PRICING SECTION -->
        <section class="p-t-40 p-b-75">
            <div class="container p-t-20">
                <h1 class="text-center ">Wir wollen Dich nicht mit unzähligen Tarifen verwirren.<br> Deshalb haben wir
                    nur <b>einen</b> Tarif. Premium!</h1>
                <div class="p-l-30 p-r-30 m-t-50">
                    <div class="row">
                        <div class="col-sm-2 p-l-5 p-r-5 "></div>
                        <div class="col-sm-4 p-l-5 p-r-5 ">
                            <div class="pricing-headingm m-t-45">
                                <div class="bg-master-light p-t-60 p-b-60">
                                    <h1 class="text-center m-b-25 font-montserrat">25 €</h1>
                                    <p class="hint-text text-center">Die Rundum Salonverwaltung -
                                        <br>Salontime ist immer Premium!</p>
                                </div>
                                <div class="bg-info p-t-20 p-b-20">
                                    <h5 class="block-title text-center text-white no-margin">Monatliche Zahlweise</h5>
                                </div>
                            </div>
                            <div class="pricing-details bg-white p-t-30 p-b-30 p-l-40 p-r-40 md-p-l-20 md-p-r-20">
                                <ul class="no-style">
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Termine
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Mitarbeiter
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Services
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Premium</span> Support
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Kunden
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Email</span>
                                        Erinnerungen
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Monatlich</span>
                                        Kündbar
                                    </li>
                                </ul>
                                <div class="block-title text-center text-white m-b-25"><a href="/signup">
                                        <button class="btn btn-danger  btn-cons" type="button"><i class="fa fa-sign-in">
                                                &nbsp;</i>Account erstellen
                                        </button>
                                    </a></div>
                                <p class="small-text hint-text">Wir sind keine Freunde von Knebelverträgen und endlosen
                                    Laufzeiten. Schreibe uns einfach eine Mail und Dein Abo endet zum Monatsende. Ganz
                                    einfach und unbürokratisch.</p>
                            </div>
                        </div>
                        <div class="col-sm-4 p-l-5 p-r-5 xs-m-t-40">
                            <div class="pricing-heading xs-p-t-10">
                                <div class="m-l-15 m-r-15 bg-success padding-10">
                                    <h5 class="block-title text-center text-white no-margin">20% Rabatt </h5>
                                </div>
                                <div class="bg-master-light p-t-60 p-b-60">
                                    <h1 class="text-center m-b-25 font-montserrat">199 €</h1>
                                    <p class="hint-text text-center">Spare satte 20% wenn
                                        <br> Du Salontime für ein Jahr buchst.</p>
                                </div>
                                <div class="bg-info p-t-20 p-b-20">
                                    <h5 class="block-title text-center text-white no-margin">Jährliche Zahlweise</h5>
                                </div>
                            </div>
                            <div class="pricing-details bg-white p-t-30 p-b-30 p-l-40 p-r-40 md-p-l-20 md-p-r-20">
                                <ul class="no-style">
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Termine
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Mitarbeiter
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Services
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Premium</span> Support
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Unbegrenzte</span>
                                        Kunden
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Email</span>
                                        Erinnerungen
                                    </li>
                                    <li class="text-black fs-16 normal m-b-25"><span class="bold">Jährlich</span>
                                        Kündbar
                                    </li>
                                </ul>
                                <div class="block-title text-center text-white m-b-25"><a href="/signup">
                                        <button class="btn btn-danger  btn-cons" type="button"><i class="fa fa-sign-in">
                                                &nbsp;</i>Account erstellen
                                        </button>
                                    </a></div>
                                <p class="small-text hint-text">Wenn Du Dich bei uns für ein Jahr registrierst, sparst
                                    Du 20%. Dein Vertrag verlängert sich automatisch, wenn du nicht vor Ablauf der
                                    Laufzeit per Mail oder Fax gekündigt hast. </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- END PRICING SECTION -->
    </section>
    <section class="p-b-85 p-t-75 ">
        <!-- BEGIN TESTIMONIALS SLIDER-->
        <div class="container" id="container">
            <div class="container">
                <!-- BEGIN TESTIMONIAL -->
                <div class="container">
                    <div class="container">
                        <div class="col-sm-8 col-sm-offset-2">
                            <h3 class="text-center">
                                Unser Ziel ist es Kunden und Salons so einfach und <br>schnell wie möglich zusammen zu
                                bringen.
                                Aus diesem Ziel geht unser Motto hervor: "Perfektion vereinfacht alles".
                                <img alt="" class="m-t-20 center" src="assets/images/signature_sample.png">
                            </h3>

                        </div>
                    </div>
                </div>
                <!-- END TESTIMONIAL -->

            </div>
            <!-- Add Navigation -->

        </div>
        <!-- END TESTIMONIALS -->
    </section>
@endsection

@section('scripts')
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="/assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script src="assets_frontend/js/custom.js" type="text/javascript"></script>
@endsection