@extends ('layouts.pages')

@section('content')
    <section class="p-t-100 sm-p-t-30 p-b-30 bg-master-lighter">
        <!-- BEGIN PRICING SECTION -->
        <section class="p-t-40 p-b-75">
            <div class="container p-t-10">
                <h1 class="text-center ">Impressum<br></h1>
                <div class="col-md-6 m-t-20">
                    <h5><b>Angaben gemäß § 5 TMG:</b></h5>Andreas Klingenberger<br>Salontime<br>Frankenring 84<br>
                    41812 Erkelenz
                </div>
                <div class="col-md-6 m-t-20"><h5><b>Kontakt:</b></h5>
                    Telefon: +49 (0) 174 2100 569<br>Telefax: +49 (0) 2431 97 59 560<br>
                    E-Mail: info@salontime.de<br>
                    <h5><b>Umsatzsteuer-ID:</b></h5>
                    Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:<br>
                    DE 999 999 999
                </div>
                <div class="col-md-12 m-t-20"><h5><b>Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:</b></h5>
                    Andreas Klingenberger<br>
                    Frankenring 84<br>
                    41812 Erkelenz<br><br>


                    <h5><b>Hinweis auf EU-Streitschlichtung:</b></h5>
                    Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit:
                    http://ec.europa.eu/consumers/odr Unsere E-Mail-Adresse finden sie oben im Impressum.
                </div>
                <div class="col-md-12 m-t-20"><h5><b>Haftungsausschluss (Disclaimer)</b></h5>
                    <h5>Haftung für Inhalte</h5>
                    Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den
                    allgemeinen
                    Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht
                    verpflichtet,
                    übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen,
                    die auf
                    eine rechtswidrige Tätigkeit hinweisen.<br>
                    Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen
                    Gesetzen
                    bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis
                    einer
                    konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden
                    wir
                    diese Inhalte umgehend entfernen.<br><br>
                    <h5>Haftung für Links</h5>
                    Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss
                    haben.
                    Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der
                    verlinkten
                    Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten
                    Seiten wurden zum
                    Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum
                    Zeitpunkt der
                    Verlinkung nicht erkennbar.<br>
                    Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte
                    einer
                    Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links
                    umgehend entfernen.<br><br>
                    <h5>Urheberrecht</h5>
                    Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem
                    deutschen
                    Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb
                    der
                    Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw.
                    Erstellers.
                    Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.
                    Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte
                    Dritter
                    beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf
                    eine
                    Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei
                    Bekanntwerden
                    von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
                </div>
            </div>
        </section>
        <!-- END PRICING SECTION -->
    </section>

@endsection

@section('scripts')
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="/assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script src="assets_frontend/js/custom.js" type="text/javascript"></script>
@endsection