@extends ('layouts.pages')

@section('content')
    <!-- START CONTACT SECTION -->
    <section class="container container-fixed-lg p-t-100 p-b-80  sm-p-t-30 sm-p-b-30">
        <div class="row">
            <div class="col-md-5 m-t-70">
                <div class="row">
                    <div class="col-sm-6 col-md-12">
                        <h4>Du brauchst Hilfe oder Unterstützung? <br>
                            Gib hier Deine Anfrage ein und wir <br>
                            helfen Dir, eine Lösung zu finden.</h4>
                    </div>
                    <div class="col-sm-6 col-md-12">
                        <br class="visible-md visible-xs">
                        <h5 class="block-title hint-text m-b-0">Salontime,<br> Inh. Andreas Klingenberger </h5>
                        <address class="text-master m-t-5">Frankenring 84
                            <br> 41812, Erkelenz
                            <br> Tel. 0174 - 2100 569
                            <br> info@salontime.de
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-md-5 m-t-70">
                <br class=" visible-xs">
                <br class="visible-xs">
                <h4 class="sm-m-t-20">Schreib uns!</h4>
                <form class="" role="form">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Vorname</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Nachname</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-default">
                        <label>Nachricht</label>
                        <textarea class="form-control" style="height:100px"
                                  placeholder="Gib hier Deine Nachricht ein"></textarea>
                    </div>
                    <p class="pull-left  small hint-text m-t-5 font-arial sm-m-t-10"></p>
                    <button class="btn btn-white font-montserrat all-caps fs-11 pull-right sm-m-t-10">Nachricht senden
                    </button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </section>
    <!-- END CONTACT SECION -->
@endsection

@section('scripts')
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="/assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script src="assets_frontend/js/custom.js" type="text/javascript"></script>
@endsection