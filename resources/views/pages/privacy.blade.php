@extends ('layouts.pages')

@section('content')
    <section class="p-t-100 sm-p-t-30 p-b-30 bg-master-lighter">
        <!-- BEGIN PRICING SECTION -->
        <section class="p-t-40 p-b-75">
            <div class="container p-t-10">
                <h1 class="text-center ">Datenschutz<br></h1>
                <div class="col-md-2"></div>
                <div class="col-md-8 center m-t-20">
                    Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Wir behandeln
                    Ihre
                    personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie
                    dieser
                    Datenschutzerklärung.<br><br>
                    Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit
                    auf
                    unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder E-Mail-Adressen) erhoben
                    werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre
                    ausdrückliche
                    Zustimmung nicht an Dritte weitergegeben.<br><br>
                    Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail)
                    Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist
                    nicht
                    möglich.<br><br>
                    <h5>Datenschutzerklärung für die Nutzung von etracker</h5>
                    Unsere Webseite nutzt den Analysedienst etracker. Anbieter ist die etracker GmbH, Erste
                    Brunnenstraße 1,
                    20459 Hamburg Germany. Aus den Daten können unter einem Pseudonym Nutzungsprofile erstellt werden.
                    Dazu können Cookies eingesetzt werden. Bei Cookies handelt es sich um kleine Textdateien, die lokal
                    im
                    Zwischenspeicher Ihres Internet-Browsers gespeichert werden. Die Cookies ermöglichen es, Ihren
                    Browser
                    wieder zu erkennen. Die mit den etracker-Technologien erhobenen Daten werden ohne die gesondert
                    erteilte
                    Zustimmung des Betroffenen nicht genutzt, Besucher unserer Website persönlich zu identifizieren und
                    werden
                    nicht mit personenbezogenen Daten über den Träger des Pseudonyms zusammengeführt.
                    Der Datenerhebung und -speicherung können Sie jederzeit mit Wirkung für die Zukunft widersprechen.
                    Um einer
                    Datenerhebung und -speicherung Ihrer Besucherdaten für die Zukunft zu widersprechen, können Sie
                    unter
                    nachfolgendem Link ein Opt-Out-Cookie von etracker beziehen, dieser bewirkt, dass zukünftig keine
                    Besucherdaten Ihres Browsers bei etracker erhoben und gespeichert werden:
                    http://www.etracker.de/privacy?et=V23Jbb
                    Dadurch wird ein Opt-Out-Cookie mit dem Namen "cntcookie" von etracker gesetzt. Bitte löschen Sie
                    diesen
                    Cookie nicht, solange Sie Ihren Widerspruch aufrecht erhalten möchten. Weitere Informationen finden
                    Sie in den
                    Datenschutzbestimmungen von etracker: http://www.etracker.com/de/datenschutz.html
                    <br><br>
                    <h5>Datenschutzerklärung für die Nutzung von Facebook-Plugins (Like-
                        Button)</h5>
                    Auf unseren Seiten sind Plugins des sozialen Netzwerks Facebook, Anbieter Facebook Inc., 1 Hacker
                    Way,
                    Menlo Park, California 94025, USA, integriert. Die Facebook-Plugins erkennen Sie an dem
                    Facebook-Logo oder
                    dem "Like-Button" ("Gefällt mir") auf unserer Seite. Eine übersicht über die Facebook-Plugins finden
                    Sie hier:
                    http://developers.facebook.com/docs/plugins/.<br>
                    Wenn Sie unsere Seiten besuchen, wird über das Plugin eine direkte Verbindung zwischen Ihrem Browser
                    und
                    dem Facebook-Server hergestellt. Facebook erhält dadurch die Information, dass Sie mit Ihrer
                    IP-Adresse
                    unsere Seite besucht haben. Wenn Sie den Facebook "Like-Button" anklicken während Sie in Ihrem
                    Facebook-
                    Account eingeloggt sind, können Sie die Inhalte unserer Seiten auf Ihrem Facebook-Profil verlinken.
                    Dadurch
                    kann Facebook den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass
                    wir als
                    Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch
                    Facebook
                    erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Facebook unter
                    http://de-de.facebook.com/policy.php.<br>
                    Wenn Sie nicht wünschen, dass Facebook den Besuch unserer Seiten Ihrem Facebook-Nutzerkonto zuordnen
                    kann, loggen Sie sich bitte aus Ihrem Facebook-Benutzerkonto aus.<br><br>
                    <h5>Datenschutzerklärung für die Nutzung von Google Analytics</h5>
                    Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics. Anbieter ist die Google
                    Inc., 1600
                    Amphitheatre Parkway Mountain View, CA 94043, USA.<br>
                    Google Analytics verwendet sog. "Cookies". Das sind Textdateien, die auf Ihrem Computer gespeichert
                    werden
                    und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten
                    Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in
                    den USA
                    übertragen und dort gespeichert.<br>
                    Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der
                    Datenschutzerklärung
                    von Google: https://support.google.com/analytics/answer/6004245?hl=de<br><br>
                    <h5>Browser Plugin</h5>
                    Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software
                    verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche
                    Funktionen
                    dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der
                    durch das
                    Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google
                    sowie
                    die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link
                    verfügbare
                    Browser-Plugin herunterladen und installieren:
                    https://tools.google.com/dlpage/gaoptout?hl=de<br><br>
                    <h5>Widerspruch gegen Datenerfassung</h5>
                    Sie können die Erfassung Ihrer Daten durch Google Analytics verhindern, indem Sie auf folgenden Link
                    klicken.
                    Es wird ein Opt-Out-Cookie gesetzt, dass das Erfassung Ihrer Daten bei zukünftigen Besuchen dieser
                    Website
                    verhindert: Google Analytics deaktivieren<br><br>
                    <h5>Auftragsdatenverarbeitung</h5>
                    Wir haben mit Google einen Vertrag zur Auftragsdatenverarbeitung abgeschlossen und setzen die
                    strengen
                    Vorgaben der deutschen Datenschutzbehörden bei der Nutzung von Google Analytics vollständig
                    um.<br><br>
                    <h5>IP-Anonymisierung</h5>
                    Wir nutzen die Funktion "Aktivierung der IP-Anonymisierung" auf dieser Webseite. Dadurch wird Ihre
                    IP-Adresse
                    von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen
                    Vertragsstaaten des
                    Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle
                    IPAdresse
                    an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser
                    Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um
                    Reports
                    über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der
                    Internetnutzung
                    verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google
                    Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google
                    zusammengeführt.<br><br>
                    <h5>Datenschutzerklärung für die Nutzung von Google+</h5>
                    Unsere Seiten nutzen Funktionen von Google+. Anbieter ist die Google Inc., 1600 Amphitheatre Parkway
                    Mountain View, CA 94043, USA.<br>
                    Erfassung und Weitergabe von Informationen: Mithilfe der Google+-Schaltfläche können Sie
                    Informationen
                    weltweit veröffentlichen. über die Google+-Schaltfläche erhalten Sie und andere Nutzer
                    personalisierte Inhalte
                    von Google und unseren Partnern. Google speichert sowohl die Information, dass Sie für einen Inhalt
                    +1
                    gegeben haben, als auch Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben.
                    Ihre +1
                    können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in
                    Suchergebnissen oder in Ihrem Google-Profil, oder an anderen Stellen auf Websites und Anzeigen im
                    Internet
                    eingeblendet werden.<br>
                    Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die Google-Dienste für Sie und andere
                    zu
                    verbessern. Um die Google+-Schaltfläche verwenden zu können, benötigen Sie ein weltweit sichtbares,
                    öffentliches Google-Profil, das zumindest den für das Profil gewählten Namen enthalten muss. Dieser
                    Name wird
                    in allen Google-Diensten verwendet. In manchen Fällen kann dieser Name auch einen anderen Namen
                    ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto verwendet haben. Die Identität
                    Ihres Google-
                    Profils kann Nutzern angezeigt werden, die Ihre E-Mail-Adresse kennen oder über andere
                    identifizierende
                    Informationen von Ihnen verfügen.<br>
                    Verwendung der erfassten Informationen: Neben den oben erläuterten Verwendungszwecken werden die von
                    Ihnen bereitgestellten Informationen gemäß den geltenden Google-Datenschutzbestimmungen genutzt.
                    Google
                    veröffentlicht möglicherweise zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer bzw.
                    gibt diese
                    an Nutzer und Partner weiter, wie etwa Publisher, Inserenten oder verbundene Websites.<br><br>
                    <h5>Datenschutzerklärung für die Nutzung von Piwik</h5>
                    Diese Website benutzt den Open Source Webanalysedienst Piwik. Piwik verwendet sog. "Cookies". Das
                    sind
                    Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der
                    Website
                    durch Sie ermöglichen. Dazu werden die durch den Cookie erzeugten Informationen über die Benutzung
                    dieser
                    Website auf unserem Server gespeichert. Die IP-Adresse wird vor der Speicherung anonymisiert.
                    Die durch den Cookie erzeugten Informationen über die Benutzung dieser Website werden nicht an
                    Dritte
                    weitergegeben. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer
                    Browser-
                    Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht
                    sämtliche
                    Funktionen dieser Website vollumfänglich werden nutzen können.<br>
                    Wenn Sie mit der Speicherung und Nutzung Ihrer Daten nicht einverstanden sind, können Sie die
                    Speicherung
                    und Nutzung hier deaktivieren. In diesem Fall wird in Ihrem Browser ein Opt-Out-Cookie hinterlegt
                    der
                    verhindert, dass Piwik Nutzungsdaten speichert. Wenn Sie Ihre Cookies löschen hat dies zur Folge,
                    dass auch
                    das Piwik Opt-Out-Cookie gelöscht wird. Das Opt-Out muss bei einem erneuten Besuch unserer Seite
                    wieder
                    aktiviert werden.<br><br>
                    <h5>Datenschutzerklärung für die Nutzung von Twitter</h5>
                    Auf unseren Seiten sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden
                    angeboten
                    durch die Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA. Durch das
                    Benutzen von
                    Twitter und der Funktion "Re-Tweet" werden die von Ihnen besuchten Webseiten mit Ihrem
                    Twitter-Account
                    verknüpft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an Twitter übertragen. Wir
                    weisen
                    darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie
                    deren
                    Nutzung durch Twitter erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung
                    von Twitter
                    unter http://twitter.com/privacy.<br>
                    Ihre Datenschutzeinstellungen bei Twitter können Sie in den Konto-Einstellungen unter
                    http://twitter.com/account/settings ändern.<br><br>
                    <h5>Datenschutzerklärung für die Nutzung von Xing</h5>
                    Unsere Webseite nutzt Funktionen des Netzwerks XING. Anbieter ist die XING AG, Dammtorstraße 29-32,
                    20354 Hamburg, Deutschland. Bei jedem Abruf einer unserer Seiten, die Funktionen von Xing enthält,
                    wird eine
                    Verbindung zu Servern von Xing hergestellt. Eine Speicherung von personenbezogenen Daten erfolgt
                    dabei
                    nach unserer Kenntnis nicht. Insbesondere werden keine IP-Adressen gespeichert oder das
                    Nutzungsverhalten
                    ausgewertet.<br>
                    Weitere Information zum Datenschutz und dem Xing Share-Button finden Sie in der Datenschutzerklärung
                    von
                    Xing unter https://www.xing.com/app/share?op=data_protection<br><br>
                    <h5>Auskunft, Löschung, Sperrung</h5>
                    Sie haben jederzeit das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen
                    Daten,
                    deren Herkunft und Empfänger und den Zweck der Datenverarbeitung sowie ein Recht auf Berichtigung,
                    Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene
                    Daten
                    können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.<br><br>
                    <h5>Cookies</h5>
                    Die Internetseiten verwenden teilweise so genannte Cookies. Cookies richten auf Ihrem Rechner keinen
                    Schaden an und enthalten keine Viren. Cookies dienen dazu, unser Angebot nutzerfreundlicher,
                    effektiver und
                    sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die
                    Ihr
                    Browser speichert.<br>
                    Die meisten der von uns verwendeten Cookies sind so genannte „Session-Cookies“. Sie werden nach Ende
                    Ihres Besuchs automatisch gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie
                    diese
                    löschen. Diese Cookies ermöglichen es uns, Ihren Browser beim nächsten Besuch wiederzuerkennen.
                    Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und
                    Cookies
                    nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen
                    sowie das
                    automatische Löschen der Cookies beim Schließen des Browser aktivieren. Bei der Deaktivierung von
                    Cookies
                    kann die Funktionalität dieser Website eingeschränkt sein.<br><br>
                    <h5>Server-Log-Files</h5>
                    Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log
                    Files, die
                    Ihr Browser automatisch an uns übermittelt. Dies sind:
                    Browsertyp/ Browserversion<br>
                    verwendetes Betriebssystem<br>
                    Referrer URL<br>
                    Hostname des zugreifenden Rechners<br>
                    Uhrzeit der Serveranfrage<br><br>
                    Diese Daten sind nicht bestimmten Personen zuordenbar. Eine Zusammenführung dieser Daten mit anderen
                    Datenquellen wird nicht vorgenommen. Wir behalten uns vor, diese Daten nachträglich zu prüfen, wenn
                    uns
                    konkrete Anhaltspunkte für eine rechtswidrige Nutzung bekannt werden.<br><br>
                    <h5>Kontaktformular</h5>
                    Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem
                    Anfrageformular
                    inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und für den
                    Fall von
                    Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.
                    Widerspruch Werbe-Mails
                    Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur übersendung von
                    nicht
                    ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. Die
                    Betreiber der
                    Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von
                    Werbeinformationen, etwa durch Spam-E-Mails, vor.<br><br>
                    <h5>Newsletterdaten</h5>
                    Wenn Sie den auf der Webseite angebotenen Newsletter beziehen möchten, benötigen wir von Ihnen eine
                    EMail-
                    Adresse sowie Informationen, welche uns die überprüfung gestatten, dass Sie der Inhaber der
                    angegebenen E-Mail-Adresse sind und mit dem Empfang des Newsletters einverstanden sind. Weitere
                    Daten
                    werden nicht erhoben. Diese Daten verwenden wir ausschließlich für den Versand der angeforderten
                    Informationen und geben sie nicht an Dritte weiter.<br>
                    Die erteilte Einwilligung zur Speicherung der Daten, der E-Mail-Adresse sowie deren Nutzung zum
                    Versand des
                    Newsletters können Sie jederzeit widerrufen , etwa über den „Austragen“-Link im Newsletter.

                </div>
                <div class="col-md-2"></div>
            </div>
        </section>
        <!-- END PRICING SECTION -->
    </section>

@endsection

@section('scripts')
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="/assets_frontend/plugins/swiper/js/swiper.jquery.min.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets_frontend/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages_frontend/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script src="assets_frontend/js/custom.js" type="text/javascript"></script>
@endsection