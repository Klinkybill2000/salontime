<nav class="header bg-header transparent-dark " data-pages="header" data-pages-header="autoresize"
     data-pages-resize-class="dark">
    <div class="container relative">
        <!-- BEGIN LEFT CONTENT -->
        <div class="pull-left">
            <!-- .header-inner Allows to vertically Align elements to the Center-->
            <div class="header-inner">
                <!-- BEGIN LOGO -->
                <a href="/">
                    <img src="assets/images/logo_black.png" width="152" height="21"
                         data-src-retina="assets/images/logo_black_2x.png" class="logo" alt="">
                    <img src="assets/images/logo_white.png" width="152" height="21"
                         data-src-retina="assets/images/logo_white_2x.png" class="alt" alt="">
                </a>
            </div>
        </div>
        <!-- BEGIN HEADER TOGGLE FOR MOBILE & TABLET -->
        <div class="pull-right">
            <div class="header-inner">
                <a href="#" class="search-toggle visible-sm-inline visible-xs-inline p-r-10" data-toggle="search"><i
                            class="fs-14 pg-search"></i></a>
                <div class="visible-sm-inline visible-xs-inline menu-toggler pull-right p-l-10"
                     data-pages="header-toggle" data-pages-element="#header">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
            </div>
        </div>
        <!-- END HEADER TOGGLE FOR MOBILE & TABLET -->
        <!-- BEGIN RIGHT CONTENT -->
        <div class="menu-content mobile-dark pull-right clearfix" data-pages-direction="slideRight" id="header">
            <!-- BEGIN HEADER CLOSE TOGGLE FOR MOBILE -->
            <div class="pull-right">
                <a href="#" class="padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10"
                   data-pages="header-toggle" data-pages-element="#header">
                    <i class=" pg-close_line"></i>
                </a>
            </div>
            <!-- END HEADER CLOSE TOGGLE FOR MOBILE -->
            <!-- BEGIN MENU ITEMS -->
            <div class="header-inner">
                <ul class="menu">
                    <li>
                        <a href="/" class="active">Home </a>
                    </li>
                    <li class="classic">
                        <a href="/features">
                            Features
                        </a>

                    </li>
                    <li>
                        <a href="/pricing">Pricing </a>
                    </li>
                    <li>
                        <a href="/support">Support </a>
                    </li>
                    <li>
                        <a href="/blog">Blog</a>
                    </li>
                    <li>
                        <a href="#"></a>
                    </li>
                    @if (Auth::guest())
                        <li>
                            <a href="/register"
                               class="btn btn-danger btn-menu fs-12 hidden-sm hidden-xs"
                               data-text="Sign Up"
                            >
                                <i class="fa fa-sign-in">&nbsp;</i> Anmelden
                            </a>
                        </li>
                        <li>
                            <a href="/login" data-text="Login">einloggen</a>
                        </li>
                    @else
                        <li>
                            <a href="{{ url('/app') }}""
                            class="btn btn-danger btn-menu fs-12 hidden-sm hidden-xs"
                            data-text="App"
                            >
                            <i class="fa fa-sign-in">&nbsp;</i> App
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}"
                               data-text="Login"
                               onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"
                            >
                                Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>

                <!-- BEGIN COPYRIGHT FOR MOBILE -->
                <div class="font-arial m-l-35 m-r-35 m-b-20 visible-sm visible-xs m-t-20">
                    <p class="fs-11 no-margin small-text p-b-20">Die beste Salonverwaltung Exklusiv nur bei Salontime
                    </p>
                    <p class="fs-11 small-text muted">Copyright &copy; 2016 Salontime</p>
                </div>
                <!-- END COPYRIGHT FOR MOBILE -->
            </div>
            <!-- END MENU ITEMS -->
        </div>
    </div>
</nav>