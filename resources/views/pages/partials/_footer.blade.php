<section class="p-b-55 p-t-75 xs-p-b-20 bg-master-darker ">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-12 xs-m-b-40">
                <img src="assets/images/logo_white.png" width="152" height="21"
                     data-src-retina="assets/images/logo_white_2x.png" class="alt" alt="">
            </div>
            <div class="col-sm-2 col-xs-6 xs-m-b-20">
                <h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10">Kontakt </h6>
                <ul class="no-style">
                    <li class="m-b-5 no-padding"><a href="/kontakt" class="link text-white ">Kontakt</a></li>
                    <li class="m-b-5 no-padding"><a href="/" class="link text-white">Newsletter</a></li>
                    <li class="m-b-5 no-padding"><a href="/register" class="link text-white">Anmelden</a></li>
                </ul>
            </div>
            <div class="col-sm-2 col-xs-6 xs-m-b-20">
                <h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10">Über Salontime</h6>
                <ul class="no-style">
                    <li class="m-b-5 no-padding"><a href="/blog" class="link text-white ">Blog</a></li>
                    <li class="m-b-5 no-padding"><a href="/" class="link text-white ">Home</a></li>
                    <li class="m-b-5 no-padding"><a href="/login" class="link text-white ">Einloggen</a></li>
                </ul>
            </div>
            <div class="col-sm-2 col-xs-6 xs-m-b-20">
                <h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10">Salontime</h6>
                <ul class="no-style">
                    <li class="m-b-5 no-padding"><a href="/features" class="link text-white ">Features</a></li>
                    <li class="m-b-5 no-padding"><a href="/pricing" class="link text-white">Preis</a></li>
                    <li class="m-b-5 no-padding"><a href="/support" class="link text-white">Support</a></li>
                </ul>
            </div>
            <div class="col-sm-2 col-xs-6 xs-m-b-20">
                <h6 class="font-montserrat text-uppercase fs-14 text-white p-b-10">Rechtliches </h6>
                <ul class="no-style">
                    <li class="m-b-5 no-padding"><a href="/impressum" class="link text-white ">Impressum</a></li>
                    <li class="m-b-5 no-padding"><a href="/agb" class="link text-white">AGB</a></li>
                    <li class="m-b-5 no-padding"><a href="/datenschutz" class="link text-white">Datenschutz</a></li>
                </ul>
            </div>


        </div>
        <p class="fs-12 hint-text p-t-10 text-white">Copyright &copy; 2016. Alle Rechte vorbehalten. </p>
    </div>
</section>