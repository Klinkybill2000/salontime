<div class="tab-pane padding-20 active slide-left" id="tab1">
    <div class="row row-same-height">
        <form method="POST" action="{{ action('SalonController@update') }}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="col-md-6 b-r b-dashed b-grey sm-b-b p-r-30">
                <h5>Your Salontime URL</h5>

                <div class="form-group form-group-default input-group{{ $errors->has('profile_url') ? ' has-error' : '' }}">
                    <label>Your booking page URL</label>

                    <input type="text" name="profile_url" class="form-control" value="{{ $salon->profile_url }}">
                    <span class="input-group-addon">.salontime.de</span>
                </div>

                @include ('errors.display', ['field' => 'profile_url'])

                <p class="text-success">
                    <i class="fa fa-share-square-o" aria-hidden="true"></i>

                    <a href="{{ action('BookingController@show', $salon->profile_url) }}" target="_blank">Jump to
                        Profile</a>
                </p>

                <h5>Contact Details</h5>

                <div class="form-group form-group-default required{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label>Salon Name</label>

                    <input
                            type="text"
                            name="name"
                            class="form-control"
                            value="{{ old('name') ?? $salon->name }}"
                            required
                    >

                    @include ('errors.display', ['field' => 'name'])
                </div>

                <div class="form-group  form-group-default required{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label>e-Mail</label>

                    <input
                            type="email"
                            name="email"
                            class="form-control"
                            value="{{ old('email') ?? $salon->email }}"
                            required
                    >

                    @include ('errors.display', ['field' => 'email'])
                </div>

                <div class="form-group  form-group-default required{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label>Phone</label>

                    <input
                            type="text"
                            name="phone"
                            class="form-control"
                            value="{{ old('phone') ?? $salon->phone }}"
                            required
                    >

                    @include ('errors.display', ['field' => 'phone'])
                </div>

                <div class="form-group form-group-default{{ $errors->has('website') ? ' has-error' : '' }}">
                    <label>Website</label>

                    <input
                            type="text"
                            name="website"
                            class="form-control"
                            value="{{ old('website') ?? $salon->website }}"
                    >

                    @include ('errors.display', ['field' => 'website'])
                </div>

                <h5>Address</h5>

                <div class="form-group form-group-default required{{ $errors->has('street') ? ' has-error' : '' }}">
                    <label>Street</label>

                    <input
                            type="text"
                            name="street"
                            class="form-control"
                            value="{{ old('street') ?? $salon->street }}"
                            required
                    >

                    @include ('errors.display', ['field' => 'street'])
                </div>

                <div class="form-group form-group-default required{{ $errors->has('zip') ? ' has-error' : '' }}">
                    <label>zip</label>
                    <input type="text" name="zip" class="form-control" value="{{ $salon->zip }}" required>

                    @include ('errors.display', ['field' => 'zip'])
                </div>

                <div class="form-group form-group-default required{{ $errors->has('city') ? ' has-error' : '' }}">
                    <label>City</label>

                    <input
                            type="text"
                            name="city"
                            class="form-control"
                            value="{{ old('city') ?? $salon->city }}"
                            required
                    >

                    @include ('errors.display', ['field' => 'city'])
                </div>
            </div>

            <div class="col-md-6 p-l-30">
                <h5>Social Media</h5>

                <div class="form-group form-group-default{{ $errors->has('facebook') ? ' has-error' : '' }}">
                    <label>Facebook Url</label>

                    <input
                            type="text"
                            class="form-control"
                            value="{{ old('facebook') ?? $salon->facebook }}"
                            name="facebook"
                    >

                    @include ('errors.display', ['field' => 'facebook'])
                </div>

                <div class="form-group  form-group-default{{ $errors->has('twitter') ? ' has-error' : '' }}">
                    <label>Twitter URL</label>

                    <input
                            type="text"
                            class="form-control"
                            value="{{ old('twitter') ?? $salon->twitter }}"
                            name="twitter"
                    >

                    @include ('errors.display', ['field' => 'twitter'])
                </div>

                <h5>Description</h5>

                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                    <textarea
                            class="form-control"
                            rows="4"
                            value="{{ old('description') ?? $salon->description }}"
                            name="description"
                    >{{ old('description') ?? $salon->description }}</textarea>

                    @include ('errors.display', ['field' => 'description'])
                </div>

                <div class="form-group">
                    <button
                    type="submit"
                    class="btn btn-primary btn-cons btn-animated from-left fa fa-clock-o pull-right"
                    >
                        <span>Save</span>
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
