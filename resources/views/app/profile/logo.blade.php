<div class="tab-pane slide-left padding-20" id="tab2">
    <div class="row row-same-height">
        <h5 class="p-l-30">Logo of Your Salon</h5>

        <div class="col-md-6 p-l-30">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Update your Logo
                    </div>

                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="config" data-toggle="modal" href="#grid-config"></a>
                        <a class="reload" href="javascript:;"></a>
                        <a class="remove" href="javascript:;"></a>
                    </div>
                </div>

                <div class="panel-body no-scroll no-padding">
                    <form
                            action="{{ url('/salon/profile/logo') }}"
                            id="addLogoForm"
                            class="dropzone no-margin"
                    >
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="fallback">
                            <input name="logo" type="image">
                        </div>
                    </form>
                </div>
            </div>
        </div>


        @if ($salon->logo)
            <div class="col-md-6 p-l-30 text-center">
                <h3>{{ $salon->name }}</h3>

                <img src="{{ asset($salon->logoPath) }}" alt="{{ $salon->name }} - logo" class="img-responsive img-thumbnail">
            </div>
        @endif

    </div>
</div>