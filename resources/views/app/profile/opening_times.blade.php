<div class="tab-pane slide-left padding-20" id="tab3">
    <div class="row row-same-height">
        <div class="col-md-5 b-r b-dashed b-grey ">
            <div class="padding-30 m-t-50">
                <h2>Set up the Salons opening Times!</h2>
                <p>Fill in the opening Times of your Salon. If your staff member have other availibility times, you can
                    change them here.</p>
                <p class="small hint-text"></p>
            </div>
        </div>

        <div class="col-md-7">
            <div class="padding-30">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="">
                            <label class="inline"><h5>Opening Times</h5></label>
                        </div>
                    </div>
                </div>

                @foreach ($salon->workingDays as $day)
                    <opening-time :plan="{{ $day }}">{{ $day->day }}</opening-time>
                @endforeach
            </div>
        </div>
    </div>
</div>