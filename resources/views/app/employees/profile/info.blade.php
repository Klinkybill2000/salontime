<div class="tab-pane padding-20 active slide-left" id="tab1">
    <div class="row row-same-height">

        <div class="col-md-5 b-r b-dashed b-grey sm-b-b">
            <div class="padding-30 ">
        <span class="thumbnail-wrapper circular inline m-t-5">
            <img
                    src="{{ asset($employee->avatar) }}"
                    alt="{{ $employee->name }}"
                    data-src="{{ asset($employee->avatar) }}"
                    data-src-retina="{{ asset($employee->avatar) }}"
                    height="75"
                    width="75"
            >
        </span>

                <br>

                <h2>Please give us some infos about your new Staffmember!</h2>

                <p>Fill the form out to Setup your Staff members and get their Statistics.</p>
            </div>
        </div>

        <div class="col-md-7">
            <form
                    class="padding-30"
                    method="POST"
                    action="{{ action('EmployeesController@update', $employee->id) }}"
                    enctype="multipart/form-data"
            >
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <div class="form-group form-group-default required{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label>Name</label>

                    <input
                            type="text"
                            name="name"
                            class="form-control"
                            value="{{ old('name') ?? $employee->name }}"
                            required
                    >

                    @include ('errors.display', ['field' => 'name'])
                </div>

                <div class="form-group  form-group-default required{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label>e-Mail</label>

                    <input
                            type="email"
                            name="email"
                            class="form-control"
                            placeholder="max.mustermann@beispiel.de"
                            value="{{ old('email') ?? $employee->email }}"
                            required
                    >

                    @include ('errors.display', ['field' => 'email'])
                </div>

                <div class="form-group form-group-default{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label>Phone</label>

                    <input
                            type="text"
                            name="phone"
                            class="form-control"
                            value="{{ old('phone') ?? $employee->phone }}"
                    >

                    @include ('errors.display', ['field' => 'phone'])
                </div>

                <div class="form-group form-group-default{{ $errors->has('avatar') ? ' has-error' : '' }}">
                    <label>Avatar</label>

                    <br>

                    <input
                            type="file"
                            name="avatar"
                            class="form-control"
                    >

                    @include ('errors.display', ['field' => 'avatar'])
                </div>

                <div class="form-group">
                    <button
                            type="submit"
                            class="btn btn-primary btn-cons btn-animated from-left fa fa-clock-o pull-right"
                    >
                        <span>Save</span>
                    </button>
                </div>

            </form>
        </div>

    </div>
</div>