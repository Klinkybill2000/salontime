@extends ('layouts.app')

@section ('content')
<div id="panel" class="m-t-50">
  <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">
    <li class="active">
      <a data-toggle="tab" href="#tab1"><i class="fa fa-user"></i> <span>Info</span></a>
    </li>
    <li class="">
      <a data-toggle="tab" href="#tab2"><i class="fa fa-clock-o"></i> <span>Working Hours</span></a>
    </li>
    <li class="">
      <a data-toggle="tab" href="#tab3"><i class="fa fa-list-ul"></i> <span>Services</span></a>
    </li>
    <li class="">
      <a data-toggle="tab" href="#tab4"><i class="fa fa-bar-chart"></i> <span>Statistics</span></a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <!-- Basic employee's info -->
    @include ('app.employees.profile.info')
  
    <!-- Employee's working hours -->
    @include ('app.employees.profile.working_hours')
    
    <!-- Employee's services -->
    @include ('app.employees.profile.services')
    
    <!-- Employee's statistics -->
    @include ('app.employees.profile.statistics')
  </div>
</div>
@endsection

@section ('scripts')
<script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}"></script>
<script src="{{ asset('assets/js/tapatar.min.js') }}"></script>
<script src="{{ asset('assets/plugins/classie/classie.js') }}"></script>
<script src="{{ asset('pages/js/pages.min.js') }}"></script>
<script>
  $('input.tapatar').tapatar({
    image_url_prefix: '/assets/img/tapatar/',
    default_image: '/assets/img/tapatar/default.svg',
    action: {
    content: 'Pick',
      onClick: function(evt) {
          if (this.image_data) {
            this.setImageData(this.image_data, true);
          }

          console.log('pepre');
      }
    },
  });
</script>
@endsection