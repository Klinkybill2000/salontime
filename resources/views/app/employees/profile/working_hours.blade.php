<div class="tab-pane slide-left padding-20" id="tab2">
    <div class="row">
        <div class="col-md-5 b-r b-dashed b-grey ">
            <div class="padding-30 m-t-50">
                <h2>Set up the working Times!</h2>
                <p>For Default we filled in basic opening Times of your Salon. If your staff member has other
                    availibility times, you can change them here.</p>
                <p class="small hint-text"></p>
            </div>
        </div>

        <div class="col-md-7">
            <div class="padding-30">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="">
                            <label class="inline"><h5>Availability</h5></label>
                        </div>
                    </div>
                </div>

                {{--<working-day-list--}}
                        {{--:working-plan="{{ $employee->workingDays }}"--}}
                        {{--:salon-working-plan="{{ $workingPlan }}"--}}
                {{-->--}}
                {{--</working-day-list>--}}

                @foreach ($employee->workingDays as $plan)
                    @if ($salon->workingDay($plan->weekday)->isWorkable)
                        <working-day
                                :plan="{{ $plan }}"
                                :salon-plan="{{ $salon->workingDay($plan->weekday) }}"
                        >
                            {{ $plan->day }}
                        </working-day>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>