<div class="tab-pane slide-left padding-20" id="tab3">
  <div class="row">
    <div class="col-md-5 b-r b-dashed b-grey ">
        <div class="padding-30 m-t-50">
            <h2>Set up the available Services!</h2>
            <p>For Default we filled in every available Service of your Salon. If your staff member provides other services, you can change them here.</p>
            <p class="small hint-text"></p>
        </div>
    </div>

    <div class="col-md-7">
      <div class="padding-30">
         
        <div class="row">
          <div class="col-sm-6">
            <div class="">
              <label class="inline"><h5>Services</h5></label>
            </div>
          </div>
        </div>
        
        @foreach ($services as $service)
          <service :employee="{{ $employee }}" :service="{{ $service }}">
            {{ $service->name }}
          </service>
        @endforeach

      </div>
    </div>
  </div>
</div>