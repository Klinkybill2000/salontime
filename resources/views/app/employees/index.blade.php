@extends('layouts.app')

@section('content')
  <div class="container-fluid container-fixed-lg bg-white">
    <!-- START PANEL -->
    <div class="panel panel-transparent">
      <div class="panel-heading">
        <div class="panel-title"><h4>Your Staff</h4>
        </div>
        <div class="pull-right">
         <div class="col-xs-12">
          <div class="col-xs-6">
          <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
          </div>
            <button id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add Staff
            </button>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <table class="table table-hover demo-table-dynamic table-responsive-block" id="tableWithSearch">
          <thead>
            <tr>
              <th>Name</th>
              <th>E-Mail</th>
              <th>Phone</th>
              <th>Activity</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($employees as $employee)
            <tr>
              <td class="v-align-middle">
                <p>{{ $employee->name }}</p>
              </td>
              <td class="v-align-middle">
                <p>{{ $employee->name }}</p>
              </td>
              <td class="v-align-middle">
                <p>{{ $employee->phone }}</p>
              </td>
              <td class="v-align-middle">
                  <div class="btn-group">
                    <a
                      href="{{ action('EmployeesController@show', $employee->id) }}"
                      class="show-modal btn btn-success"
                    >
                      <i class="fa fa-pencil"></i>
                    </a>
                    
                    <a class="btn btn-success" onclick="event.preventDefault(); document.getElementById('destroy-employee-form-{{ $employee->id }}').submit();">
                      <i class="fa fa-trash-o"></i>
                    </a>

                    <form id="destroy-employee-form-{{ $employee->id }}"
                          action="{{ action('EmployeesController@destroy', $employee->id) }}"
                          method="POST"
                          style="display: none;"
                    >
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                  </div>
               </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <!-- END PANEL -->
  </div>

  <div class="row">
    
  </div>

@include ('app.employees.modals.add_employee')
<employee-modal></employee-modal>
@endsection

@section ('scripts')
<script type="text/javascript" src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/lodash.min.js') }}"></script>
<script src="{{ asset('assets/js/datatables.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}" type="text/javascript"></script>
@endsection