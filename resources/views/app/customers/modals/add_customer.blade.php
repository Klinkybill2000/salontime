<div class="modal fade slide-up disable-scroll in"
     id="addNewAppModal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="addNewAppModal"
     aria-hidden="true"
>
  <div class="modal-dialog">
    <div class="modal-content-wrapper">
      <div class="modal-content">
      <div class="modal-header clearfix ">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
        <h4 class="p-b-5"><span class="semi-bold">New</span> Customer</h4>
      </div>
      <div class="modal-body">
        <p class="small-text">Create a new Customer using this form, make sure you fill them all</p>
        <form role="form" method="POST" action="{{ action('CustomersController@store') }}">
          {{ csrf_field() }}

          <div class="row">
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>Name</label>
                <input id="appName" type="text" class="form-control" placeholder="Customer Name" name="name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group form-group-default">
                <label>E-Mail</label>
                <input id="appDescription" type="text" class="form-control" placeholder="Tell us his E-Mail" name="email">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group form-group-default">
                <label>Phone</label>
                <input id="appPrice" type="text" class="form-control" placeholder="eg. 0221-772565" name="phone">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group form-group-default">
                <label>Note</label>
                <input id="appNotes" type="text" class="form-control" placeholder="like favorite color" name="note">
              </div>
            </div>
          </div>

           <div class="modal-footer">
            <button id="add-app" type="submit" class="btn btn-primary btn-cons">Add</button>
            <button class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>