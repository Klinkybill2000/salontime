<!-- BEGIN SIDEBAR-->
<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="{{ asset('assets/img/logo_white.png') }}" alt="logo" class="brand"
             data-src="{{ asset('assets/img/logo_white.png') }}"
             data-src-retina="{{ asset('assets/img/logo_white_2x.png') }}" width="85" height="12">
        <div class="sidebar-header-controls">
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar">
                <i class="fa fs-12"></i>
            </button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li class="m-t-30">
                <a href="{{ action('DashboardController@index') }}" class="detailed">
                    <span class="title">Dashboard</span>
                </a>

                <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
            </li>

            <li class="">
                <a href="{{ action('CalendarController@index') }}">
                    <span class="title">Calendar</span>
                </a>

                <span class="icon-thumbnail "><i class="fa fa-calendar"></i></span>
            </li>

            <li class="">
                <a href="{{ action('EmployeesController@index') }}">
                    <span class="title">Employees</span>
                </a>

                <span class="icon-thumbnail "><i class="fa fa-briefcase"></i></span>
            </li>

            <li class="">
                <a href="{{ action('CustomersController@index') }}">
                    <span class="title">Customers</span>
                </a>

                <span class="icon-thumbnail "><i class="fa fa-users"></i></span>
            </li>

            <li class="">
                <a href="{{ action('ServicesController@index') }}">
                    <span class="title">Services</span>
                </a>

                <span class="icon-thumbnail "><i class="fa fa-truck"></i></span>
            </li>

            <li class="">
                <a href="{{ action('SalonController@index') }}">
                    <span class="title">Profile</span>
                </a>

                <span class="icon-thumbnail "><i class="fa fa-diamond"></i></span>
            </li>
            <!-- <li class="">
              <a href="javascript:;">
                <span class="title">Profile</span>
                <span class=" arrow"></span>
              </a>
              <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
              <ul class="sub-menu">
                <li class="">
                  <a href="/configure">Configure</a>
                  <span class="icon-thumbnail">Co</span>
                </li>
                <li class="">
                  <a href="#">Sub Page 2</a>
                  <span class="icon-thumbnail">sp</span>
                </li>
                <li class="">
                  <a href="#">Sub Page 3</a>
                  <span class="icon-thumbnail">sp</span>
                </li>
              </ul>
            </li> -->
            <!--       <li class="">
                    <a href="javascript:;">
                      <span class="title">Settings</span>
                      <span class=" arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-settings_small"></i></span>
                    <ul class="sub-menu">
                      <li class="">
                        <a href="#">Sub Page 1</a>
                        <span class="icon-thumbnail">sp</span>
                      </li>
                      <li class="">
                        <a href="#">Sub Page 2</a>
                        <span class="icon-thumbnail">sp</span>
                      </li>
                      <li class="">
                        <a href="#">Sub Page 3</a>
                        <span class="icon-thumbnail">sp</span>
                      </li>
                    </ul>
                  </li> -->
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->
