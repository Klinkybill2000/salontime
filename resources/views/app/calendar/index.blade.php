@extends ('layouts.app')

@section ('links')
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css">
@endsection

@section ('content')
    <div class="padding-30">
        <calendar :events="{{ $appointments }}"></calendar>

        <calendar-modal></calendar-modal>
    </div>
@endsection

@section ('scripts')
<script src="{{ asset('assets/js/scripts.js') }}"></script>
@endsection