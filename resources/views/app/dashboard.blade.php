@extends('layouts.app')

@section('content')
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="row">
            <div class="col-xs-10 col-md-4 col-lg-4 no-padding">
                @include ('app.widgets.weekly_appointments')
            </div>
            <div class="col-xs-10 col-md-4 col-lg-4 no-padding">
                @include ('app.widgets.weekly_sale')
            </div>
            <div class="col-xs-10 col-md-4 col-lg-4 no-padding">
                @include ('app.widgets.overall_sale')
            </div>
        </div>
    </div>
@endsection