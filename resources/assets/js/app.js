/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

// Custom Filters
Vue.filter('time', function(time){
    return moment(time).format('HH:mm');
});

Vue.filter('dateForHumans', function(date){
    return moment(date).format('dddd, MMMM Do YYYY');
});

// Custom Components
Vue.component('CustomerModal', require('./components/CustomerModal.vue'));
// Vue.component('EmployeeModal', require('./components/EmployeeModal.vue'));

// Services
Vue.component('ServiceProvider', require('./components/services/ServiceProvider.vue'));

// Employees
Vue.component('Service', require('./components/employees/Service.vue'));
Vue.component('WorkingDay', require('./components/employees/WorkingDay.vue'));

// Salon
Vue.component('OpeningTime', require('./components/salon/OpeningTime.vue'));

// Calendar
Vue.component('Calendar', require('./components/Calendar.vue'));

Vue.component('CalendarModal', require('./components/CalendarModal.vue'));
Vue.component('Alert', require('./components/Alert.vue'));

// Booking
Vue.component('BookAppointment', require('./components/booking/BookAppointment.vue'));

const app = new Vue({
    el: '#app'
});
