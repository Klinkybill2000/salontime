class Event {
    /**
     * Create an new Event constructor
     */
    constructor() {
        this.vue = new Vue();
    }

    /**
     * Fire an event
     *
     * @param event
     * @param data
     */
    fire(event, data = null) {
        this.vue.$emit(event, data);
    }

    /**
     * Fire an alert event
     *
     * @param message
     * @param type
     */
    fireAlert(status, type = 'success') {
        this.fire('alert', { status, type });
    }

    /**
     * Listen for an event
     *
     * @param event
     * @param callback
     */
    listen(event, callback) {
        this.vue.$on(event, callback);
    }
}

export default Event;
