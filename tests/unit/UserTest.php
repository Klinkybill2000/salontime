<?php

use App\{User, Salon};
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function it_can_have_attributes()
    {
        $password = bcrypt('password');

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => $password
        ]);

        $this->assertEquals('John Doe', $user->name);
        $this->assertEquals('john@example.com', $user->email);
        $this->assertEquals($password, $user->password);
    }

    /** @test */
    function it_can_have_the_salon_role()
    {
        $user = factory(User::class)->create();
        $salon = Salon::first();

        $user->userable()->associate($salon);

        $this->assertEquals('App\Salon', $user->userable_type);
    }

    /** @test */
    function it_can_fetch_associated_salon()
    {
        $user = factory(User::class)->create();
        $salon = Salon::first();

        $user->salon()->associate($salon);

        $this->assertEquals($salon->name, $user->fresh()->salon->name);
    }
}
