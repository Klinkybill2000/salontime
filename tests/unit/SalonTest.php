<?php

use App\{User, Salon, Service, Employee, WorkingDay};
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SalonTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function it_can_add_a_new_service()
    {
        $salon = factory(User::class)->create()->salon;

        $service = new Service([
            'name' => 'haircuts',
            'price' => 25,
            'duration' => 30,
            'description' => 'A short description.'
        ]);

        $salon->addService($service);

        $this->assertCount(1, $salon->services);
    }
    
    /** @test */
    function it_can_fetch_a_specific_service_by_its_id()
    {
        $salon = factory(User::class)->create()->salon;

        $service = $salon->addService(new Service([
            'name' => 'texture',
            'price' => 20,
            'duration' => 30,
            'description' => 'A short description.'
        ]));

        $this->assertEquals($service->name, $salon->service($service->id)->name);
    }

    /** @test */
    function it_can_hire_a_new_employeement()
    {
        $salon = factory(User::class)->create()->salon;

        $employee = new Employee([
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'phone' => '123321123'
        ]);

        $employee = $salon->hire($employee);

        $this->seeInDatabase('employees', [
            'email' => 'john@example.com',
            'phone' => 123321123
        ]);
    }

    /** @test */
    function it_can_fetch_the_specefied_working_day()
    {
        $salon = factory(User::class)->create()->salon;

        $this->createWorkingDays($salon);

        $this->assertEquals('17:00:00', $salon->workingDay(2)->work_end);
    }

    /** @test */
    function it_can_edit_a_working_day()
    {
        $salon = factory(User::class)->create()->salon;

        $workingPlan = $this->createWorkingDays($salon);

        $workingPlan->work_end = '19:00:00';

        $salon->editWorkingHours(2, $workingPlan->toArray());

        $this->seeInDatabase('working_days', [
            'weekday' => 2,
            'work_end' => '19:00:00',
            'workable_id' => 1
        ]);
    }

    /** @test */
    function it_can_save_a_new_logo()
    {
        $salon = factory(User::class)->create()->salon;

        $path = '/public/logos/some-logo.jpg';

        $salon->saveLogo($path);

        $this->assertEquals($path, $salon->logo);
    }

    /** @test */
    function it_can_fetch_a_sym_linked_logo_path()
    {
        $salon = factory(User::class)->create()->salon;

        $path = '/public/logos/some-logo.jpg';

        $salon->saveLogo($path);

        $this->assertEquals('/storage/logos/some-logo.jpg', $salon->logoPath);
    }

    /**
     * Generate the salon's working days
     *
     * @param $salon
     */
    protected function createWorkingDays($salon)
    {
        $workingPlan = new WorkingDay;

        $workingPlan->weekday = 2; // Tuesday
        $workingPlan->work_start = '09:00:00';
        $workingPlan->work_end = '17:00:00';
        $workingPlan->workable_id = $salon->id;
        $workingPlan->workable_type = 'App\Salon';

        $workingPlan->save();

        return $workingPlan;
    }
}
