<?php

class HelpersTest extends TestCase
{
    /**
     * @test
     */
    public function it_transforms_weekday_number_into_actual_day_name()
    {
        $day = dayOfWeek(1);

        $this->assertEquals('monday', $day);
    }

    /**
     * @test
     */
    public function it_transforms_weekday_number_into_actual_day_name_but_as_an_abbreviation()
    {
        $day = dayOfWeek(1, true);

        $this->assertEquals('mon', $day);
    }

    /** @test */
    function it_can_give_you_a_number_that_is_the_closest_to_given_in_an_array()
    {
        $numbers = [100, 200, 300, 400, 500];

        $result = closestToNumber(260, $numbers);

        $this->assertEquals(300, $result);
    }

    /** @test */
    function it_can_check_if_the_current_route_action_is_equal_to_nav_route_action()
    {
        $action = 'ServicesController@index';

        Route::shouldReceive('currentRouteAction')
            ->once()
            ->andReturn('App\Http\Controllers\ServicesController@index');

        $this->assertEquals('active', isActiveRouteAction($action));
    }
}
