<?php

use App\{User, Salon};
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddServiceTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function salon_owner_can_add_a_service()
    {
        $user = factory(User::class)->create();
        $salon = factory(Salon::class)->create();

        $user->salon()->associate($salon);

        $this->actingAs($user)
             ->post('/salon/services', [
                'name' => 'Haircuts',
                'duration' => 3000,
                'price' => 2500,
                'description' => 'A short description'
             ]);

        $this->assertResponseStatus(302);
    }
}
