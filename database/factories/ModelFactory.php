<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('password'),
        'remember_token' => str_random(10),
        'userable_id' => factory(App\Salon::class)->create()->id,
        'userable_type' => 'App\Salon'
    ];
});

$factory->define(App\Salon::class, function (Faker\Generator $faker) {
    $companyName = $faker->company;
    $companySlug = lcfirst(preg_split('/[\s-,]+/ ', $companyName)[0]);

    return [
        'profile_url' => $companySlug,
        'name' => $companyName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'website' => $faker->url,
        'city' => $faker->city,
        'zip' => $faker->postCode,
        'street' => $faker->streetAddress,
        'description' => $faker->text(160),
        'facebook' => 'https://facebook.com/' . $companySlug,
        'twitter' => 'https://twitter.com/' . $companySlug,
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'Salon_id' => App\Salon::first()->id,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'note' => $faker->sentence
    ];
});

$factory->define(App\Employee::class, function (Faker\Generator $faker) {
    return [
        'Salon_id' => App\Salon::first()->id,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber
    ];
});