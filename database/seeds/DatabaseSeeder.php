<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('production')) {
            die('I just stopped you getting fired!');
        }

        $tables = [
            'users',
            'salons',
            'customers',
            'employee_service',
            'employees',
            'services',
            'working_days',
            'appointments'
        ];

        if ($this->isMysql()) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        $this->call(InitialTablesSeeder::class);

        if ($this->isMysql()) {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    /**
     * Determine if the default database is mysql
     */
    protected function isMysql()
    {
        return env('DB_CONNECTION') == 'mysql';
    }
}
