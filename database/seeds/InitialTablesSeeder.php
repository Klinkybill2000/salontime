<?php

use App\{User, Salon, Customer, Employee, Service, WorkingDay, Appointment};
use Illuminate\Database\Seeder;

class InitialTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Salon
        factory(User::class)->create();

        $salon = Salon::first();

        // Create Employees
        $employees = factory(Employee::class, 5)->create();

        // Create services
        $services = [
            ['name' => 'haircuts', 'description' => 'Some description.', 'price' => '30', 'duration' => 20 ],
            ['name' => 'color', 'description' => 'Some description.', 'price' => '20', 'duration' => 30 ],
            ['name' => 'textures', 'description' => 'Some description.', 'price' => '25', 'duration' => 15 ],
            ['name' => 'massage', 'description' => 'Some description.', 'price' => '35', 'duration' => 25 ],
            ['name' => 'facials', 'description' => 'Some description.', 'price' => '15', 'duration' => 35 ],
        ];

        foreach ($services as $service) {
            Service::create([
                'Salon_id' => $salon->id,
                'name' => $service['name'],
                'description' => $service['description'],
                'price' => $service['price'],
                'duration' => $service['duration'],
            ]);
        }

        $services = $salon->services;

        foreach ($employees as $employee) {
            foreach (range(1, rand(2, 5)) as $serviceId) {
                $employee->services()->attach($serviceId);
            }
        }

        // Create Customers
        factory(Customer::class, 5)->create();

        $days = [
            0 => [
                'start' => '00:00:00',
                'end' => '00:00:00'
            ],
            1 => [
                'start' => '09:00:00',
                'end' => '14:00:00'
            ],
            2 => [
                'start' => '09:00:00',
                'end' => '17:00:00'
            ],
            3 => [
                'start' => '09:00:00',
                'end' => '17:00:00'
            ],
            4 => [
                'start' => '09:00:00',
                'end' => '15:00:00'
            ],
            5 => [
                'start' => '09:00:00',
                'end' => '17:00:00'
            ],
            6 => [
                'start' => '09:00:00',
                'end' => '13:00:00'
            ]
        ];

        /**
         * Add working plan for Salon
         */
        foreach ($days as $day => $plan) {
            $workingPlan = new WorkingDay;

            $workingPlan->weekday = $day;

            $workingPlan->work_start = date('H:i:s', strtotime($plan['start']));
            $workingPlan->work_end = date('H:i:s', strtotime($plan['end']));

            $workingPlan->workable_id = 1;
            $workingPlan->workable_type = 'App\Salon';
            
            $workingPlan->save();
        }

        /**
         * Add working plan for employees
         */
        foreach (Employee::all() as $employee) {

            foreach ($days as $day => $plan) {
                $workingPlan = new WorkingDay;

                $workingPlan->weekday = $day;

                $workingPlan->work_start = date('H:i:s', strtotime($plan['start']));
                $workingPlan->work_end = date('H:i:s', strtotime($plan['end']));

                $workingPlan->workable_id = $employee->id;
                $workingPlan->workable_type = 'App\Employee';
                
                $workingPlan->save();
            }
        }

        // Appointments
        Appointment::create([
            'salon_id' => 1,
            'service_id' => rand(1, 5),
            'employee_id' => rand(1, 5),
            'customer_id' => 1,
            'start' => date('Y-m-d H:i:s', strtotime('2016-12-19 09:00')),
            'end' => date('Y-m-d H:i:s', strtotime('2016-12-19 09:30'))
        ]);

        Appointment::create([
            'salon_id' => 1,
            'service_id' => rand(1, 5),
            'employee_id' => rand(1, 5),
            'customer_id' => 1,
            'start' => date('Y-m-d H:i:s', strtotime('2016-12-19 11:00')),
            'end' => date('Y-m-d H:i:s', strtotime('2016-12-19 11:30'))
        ]);

        Appointment::create([
            'salon_id' => 1,
            'service_id' => rand(1, 5),
            'employee_id' => rand(1, 5),
            'customer_id' => 1,
            'start' => date('Y-m-d H:i:s', strtotime('2016-12-19 12:00')),
            'end' => date('Y-m-d H:i:s', strtotime('2016-12-19 12:30'))
        ]);

        Appointment::create([
            'salon_id' => 1,
            'service_id' => rand(1, 5),
            'employee_id' => rand(1, 5),
            'customer_id' => 1,
            'start' => date('Y-m-d H:i:s', strtotime('2016-12-22 12:00')),
            'end' => date('Y-m-d H:i:s', strtotime('2016-12-22 12:30'))
        ]);

        Appointment::create([
            'salon_id' => 1,
            'service_id' => rand(1, 5),
            'employee_id' => rand(1, 5),
            'customer_id' => 1,
            'start' => date('Y-m-d H:i:s', strtotime('2016-12-22 12:00')),
            'end' => date('Y-m-d H:i:s', strtotime('2016-12-22 12:30'))
        ]);

        Appointment::create([
            'salon_id' => 1,
            'service_id' => rand(1, 5),
            'employee_id' => rand(1, 5),
            'customer_id' => 1,
            'start' => date('Y-m-d H:i:s', strtotime('2016-12-23 12:00')),
            'end' => date('Y-m-d H:i:s', strtotime('2016-12-23 12:30'))
        ]);
    }
}
