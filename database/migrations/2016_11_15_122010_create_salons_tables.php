<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('email');

            $table->string('phone');
            $table->string('website')->nullable();

            $table->string('street');
            $table->string('zip');
            $table->string('city');

            $table->string('description', 160)->nullable();

            $table->string('logo')->default('public/logos/default.png');
            $table->string('profile_url')->nullable();

            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();

            $table->timestamps();
        });

        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('salon_id')->unsigned();
            $table->foreign('salon_id')->references('id')->on('salons')->onDelete('cascade');

            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();

            $table->string('avatar')->default('default.png');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
        Schema::drop('salons');
    }
}
