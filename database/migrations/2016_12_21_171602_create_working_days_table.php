<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkingDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_days', function (Blueprint $table) {
            $table->increments('id');

            $table->enum('weekday', [0, 1, 2, 3, 4, 5, 6]);

            $table->time('work_start')->nullable();
            $table->time('work_end')->nullable();

            $table->morphs('workable');

            $table->timestamps();

            $table->unique(['workable_id', 'workable_type', 'weekday']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('working_days');
    }
}
